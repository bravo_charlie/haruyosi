<?php

namespace App\Http\Controllers;

use App\Gallery;
use App\staff;
use App\Service;
use App\Testimonial;
use App\Slider;
use App\Experience;
use App\Blog;
use App\Gallerycategory;
use App\Blogcategory;
use Illuminate\Http\Request;

class MainPageController extends Controller
{
    public function index(){
        $gallerycategories = Gallerycategory::latest()->get();
        $sliders = Slider::latest()->get();
        $testimonials = Testimonial::latest()->get();
        $services = Service::all();
        $gallery = Gallery::latest()->get();
        $experiences = experience::all();
        $staffs = Staff::all();
    	return view ('new', compact('sliders','staffs','testimonials','services','gallery','gallerycategories','experiences'));
    }
    public function aboutPage(){
        $staffs = Staff::latest()->get();
        $staffid = Staff::all();
    	return view ('newabout',compact('staffs','staffid'));
    }
    public function servicePage(){
        $services = Service::latest()->get();
        return view ('services',compact('services'));
    }

    public function  galleryPage()
    {
    	$gallery = Gallery::latest()->get();
        $gallerycategories = Gallerycategory::latest()->get();
    	return view ('newgallery', compact('gallery','gallerycategories'));
    }
    public function  blogIndex()
    {
        $blogs = Blog::latest()->paginate(4);
        $gallery = Gallery::latest()->get();
        $blogcategories = Blogcategory::latest()->get();
        return view ('newblog', compact('blogs','blogcategories','gallery'));
    }
    public function  blogCategoryPage($id)
    {
        $blogCatName = Blogcategory::findOrFail($id)->title;
        $gallery = Gallery::latest()->get();
        $blogcategories = Blogcategory::latest()->get();
        $blogs = Blog::latest()->paginate();
        return view ('newblog-category',compact('blogs','blogcategories', 'id', 'blogCatName','gallery'));
    }
    public function blogDetail($id)
    {
        $blogs = Blog::findOrFail($id);
        $gallery = Gallery::latest()->get();
        $blogcategories = Blogcategory::latest()->get();
        $allblogs = Blog::latest()->paginate();
        return view ('blog-post',compact('blogs','blogcategories','allblogs','gallery'));
    }
    public function contact(){
        return view ('contacts');
    }
    public function trainingPage(){
        return view ('training');
    }
}
