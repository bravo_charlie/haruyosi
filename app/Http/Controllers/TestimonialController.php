<?php

namespace App\Http\Controllers;

use App\Testimonial;
use Illuminate\Http\Request;

class TestimonialController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $testimonials = Testimonial::all();
        return view ('dashboard.testimonial.index',compact('testimonials'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('dashboard.testimonial.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $testimonials = new Testimonial();
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg|'
        ]);
        $testimonials->name = $request->name;
        $testimonials->description = $request->description;
        if(file_exists($request->file('image'))){
            $image = "testimonials".time().'.'.$request->file('image')->getclientOriginalName();
            $location = public_path('uploads');
            $request->file('image')->move($location, $image);
            $testimonials->image = $image;
        }
        else{
            $testimonials->image = 'default-thumbnail.png';
        }        
        $testimonials->save();
        return redirect('/home/testimonials');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Testimonial  $Testimonial
     * @return \Illuminate\Http\Response
     */
    public function show(Testimonial $Testimonial)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Testimonial  $Testimonial
     * @return \Illuminate\Http\Response
     */
    public function edit(Testimonial $Testimonial,$id)
    {
        $testimonials = Testimonial::findOrFail($id);
        return view ('dashboard.testimonial.edit',compact('testimonials'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Testimonial  $Testimonial
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Testimonial $Testimonial,$id)
    {
        $testimonials = Testimonial::findOrFail($id);
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg|'
        ]);
        $testimonials->name = $request->name;
        $testimonials->description = $request->description;
        if(file_exists($request->file('image'))){
            $image = "testimonials".time().'.'.$request->file('image')->getclientOriginalName();
            $location = public_path('uploads');
            $request->file('image')->move($location, $image);
            $testimonials->image = $image;
        }
        else{
            $testimonials->image = $testimonials->image;
        }        
        $testimonials->save();
        return redirect('/home/testimonials');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Testimonial  $Testimonial
     * @return \Illuminate\Http\Response
     */
    public function destroy( Testimonial $Testimonial,$id)
    {
        $testimonials = Testimonial::findOrFail($id) ->delete();
        return redirect()->back();
    }
}
