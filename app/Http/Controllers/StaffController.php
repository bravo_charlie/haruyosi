<?php

namespace App\Http\Controllers;

use App\staff;
use Illuminate\Http\Request;

class StaffController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $staffs = Staff::all();
        return view('dashboard.staffs.index', compact('staffs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.staffs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $staffs = new Staff();
        $request->validate([
            'name' => 'required',
            'designation' => 'required',
            'description' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg'
        ]);
        $staffs->name =$request->name;
        $staffs->designation =$request->designation;
        $staffs->description =$request->description;
        $staffs->facebook_link =$request->facebook_link;
        $staffs->twitter_link =$request->twitter_link;
        $staffs->instagram_link =$request->instagram_link;
        $staffs->mail =$request->mail;
        if(file_exists($request->file('image'))){
            $image = "staffs".time().'.'.$request->file('image')->getclientOriginalName();
            $location = public_path('uploads');
            $request->file('image')->move($location, $image);
            $staffs->image = $image;
        }
        else{
            $staffs->image = 'default-thumbnail.png';
        }
        $staffs->save();
        return redirect('/home/staffs');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\staff  $staff
     * @return \Illuminate\Http\Response
     */
    public function show(staff $staff)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\staff  $staff
     * @return \Illuminate\Http\Response
     */
    public function edit(staff $staff, $id)
    {
        $staffs = Staff::findOrFail($id);
        return view ('dashboard.staffs.edit',compact('staffs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\staff  $staff
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, staff $staff,$id)
    {
        $staffs = Staff::findOrFail($id);
        $request->validate([
            'name' => 'required',
            'designation' => 'required',
            'description' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg'
        ]);
        $staffs->name =$request->name;
        $staffs->designation =$request->designation;
        $staffs->description =$request->description;
        $staffs->facebook_link =$request->facebook_link;
        $staffs->twitter_link =$request->twitter_link;
        $staffs->instagram_link =$request->instagram_link;
        $staffs->mail =$request->mail;
        if(file_exists($request->file('image'))){
            $image = "staffs".time().'.'.$request->file('image')->getclientOriginalName();
            $location = public_path('uploads');
            $request->file('image')->move($location, $image);
            $staffs->image = $image;
        }
        else{
            $staffs->image = $staffs->image;
        }
        $staffs->save();
        return redirect('home/staffs');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\staff  $staff
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $staffs = Staff::findOrFail($id)->delete();
    return redirect()->back();
    }
}
