<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;	

class SendMailController extends Controller
{
    function send(Request $request)
    {
     $this->validate($request, [
      'name'     =>  'required',
      'sec_name'     =>  'required',
      'email'  =>  'required|email',
      'phone' =>  'required',
      'message' =>  'required'
     ]);

        $data = array(
            'name'      =>  $request->name,
            'sec_name'      =>  $request->sec_name,
            'email'   =>   $request->email,
            'phone'   =>   $request->phone,
            'message'   =>   $request->message
        );
        
        $txt1 = '<html>
        <head>  
        </head>
        <body>
                    <p>Hi, This is '. $data['name'] .' '. $data['sec_name'] .'.</p>
                    <p>Email:'. $data['email'] .'<br><br>Phone:'. $data['phone'] .'</p>
                    <p>Message:<br>'. $data['message'] .'</p>
                    <p>It would be appriciative, if i receive the reply soon.</p>
        </body>
        </html>';       

        $to = "azizdulal.ad@gmail.com";
        $subject = "Inquiry";

        $headers = "From:Haruyosi.com\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=ISO-8859-1\r\n";


        


         $result=   mail($to,$subject,$txt1,$headers);
                 return back()->with('success','Thanks for contacting us!');
        }

        function subscribe(Request $request)
    {
     $this->validate($request, [
      'email'  =>  'required|email'
     ]);

        $sdata = array(
            'email'   =>   $request->email
        );
        
        $txt2 = '<html>
        <head>  
        </head>
        <body>
                    <p>Hi, This is '. $sdata['email'] .'</p>
                    <p>I want to subscribe for the newsletter.</p>
                    <p>It would be appriciative, if i receive the subscription soon.</p>
        </body>
        </html>';       

        $to = "azizdulal.ad@gmail.com";
        $subject = "Subscription Inquiry";

        $headers = "From:Haruyosi.com\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=ISO-8859-1\r\n";


        


         $result=   mail($to,$subject,$txt2,$headers);
                 return back()->with('success','Thanks for subscribing us!');
        }

}
