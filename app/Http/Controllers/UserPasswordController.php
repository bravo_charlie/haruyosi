<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Http\Request;

class UserPasswordController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $users = User::all();
        return view ('dashboard.users.index',compact('users'));
    }
    public function create()
    {
        return view ('dashboard.users.create');
    }
    public function edit($id)
    {
        $users = User::findOrFail($id);
        return view ('dashboard.users.edit',compact('users'));
    }
    public function store(Request $request)
    {
        $users = new User();
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
        ]);
        return User::create([
            'name' => $users['name'],
            'email' => $users['email'],
            'password' => Hash::make($users['password']),
        ]);
        // return redirect('/home/users');
    }
    public function update(Request $request,$id)
    {
        $users = User::findOrFail($id);
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        $users->name = $request->name;
        $users->email = $request->email;
        $users->password = $request->password->Hash::make($data['password']);
        $users->save();
        return redirect('/home/users');
    }
    public function destroy(Request $request,$id)
    {
        $users = User::findOrFail($id)->delete();
        return redirect()->back();
    }
}
