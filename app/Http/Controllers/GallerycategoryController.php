<?php

namespace App\Http\Controllers;

use App\Gallerycategory;
use App\Gallery;
use Illuminate\Http\Request;

class GallerycategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gallerycategories = Gallerycategory::latest()->get();
        return view('dashboard.gallery.gallery-category.index',compact('gallerycategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('dashboard.gallery.gallery-category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'title'=> ['required', 'min:3']
        ]);

        $gallerycategories = new Gallerycategory();

        $gallerycategories->title = request('title');

        $gallerycategories->save();

        return redirect('/home/gallery-category');    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Gallerycategory  $gallerycategory
     * @return \Illuminate\Http\Response
     */
    public function show(Gallerycategory $gallerycategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Gallerycategory  $gallerycategory
     * @return \Illuminate\Http\Response
     */
    public function edit(Gallerycategory $gallerycategory,$id)
    {
        $gallerycategories = Gallerycategory::findOrFail($id);
        return view ('/dashboard/gallery/gallery-category/edit',compact('gallerycategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Gallerycategory  $gallerycategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Gallerycategory $gallerycategory,$id)
    {
        $gallerycategories = Gallerycategory::findOrFail($id);
        request()->validate([
            'title'=> ['required', 'min:3']
        ]);

        $gallerycategories->title = request('title');

        $gallerycategories->save();

        return redirect('/home/gallery-category');  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Gallerycategory  $gallerycategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gallerycategories = Gallerycategory::findOrFail($id)->delete();
        return redirect()->back();
    }
}
