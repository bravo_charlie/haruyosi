@extends('layouts.dashboard.main-app', ['title' => 'Nopage Ahead'],['discription'=> 'Flower Website Our Flower brings unique and custom Flower designs to all customers of LA area.'])

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="my-text">
        <h2>Not Allowed.<br>Please Go Back <br>!!!<br>(<b>Admin</b>) </h2>
        <div>
            <a href="/"><button class="btn btn-primary btn-arrow-left">Go Back</button></a>
        </div>
    </div>
    </div>
</div>
@endsection
