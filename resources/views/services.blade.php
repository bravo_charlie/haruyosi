 @extends('layouts.new.app', ['title' => 'Services'],['discription'=> 'Flower Website Our Flower brings unique and custom Flower designs to all customers of LA area.'])

@section('content')
<section class="section-page-title" style="background-image: url(images/page-title-2-1920x305.jpg); background-size: cover;">
  <div class="container">
    <h1 class="page-title">Services</h1>
  </div>
</section>
<section class="breadcrumbs-custom">
  <div class="container">
    <ul class="breadcrumbs-custom-path">
      <li><a href="/">Home</a></li>
      <li class="active">Services</li>
    </ul>
  </div>
</section>
<section class="section section-lg bg-default">
  <div class="container">
    <div class="row row-50 align-items-lg-center justify-content-xl-between">
      <div class="col-lg-6">
        <div class="block-xs">
          <h2 class="heading-decorate">Why Clients <br><span class="divider"></span>Choose Us
          </h2>
          <p class="big text-gray-800">Cras ut vestibulum tortor. In in nisi sit amet metus varius pulvinar in vitae ipsum nec mi sollicitudin luctus aliquet a, accumsan.</p>
          <p>In ante sapien, dapibus luctus aliquet a, accumsan sit amet dolor. Mauris id facilisis dolor. Donec malesuada, est eu dignissim eleifend, est nulla dignissim nisl, a facilisis ipsum.</p><a class="button button-default-outline" href="/contacts">Book Now</a>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="box-images-classic">
          <div class="row row-30">
            <div class="col-6">
              <div class="box-image-item" data-parallax-scroll="{&quot;y&quot;: 0, &quot;x&quot;: -20,  &quot;smoothness&quot;: 30 }"><img src="images/services-2-270x458.jpg" alt="" width="270" height="458"/>
              </div>
            </div>
            <div class="col-6">
              <div class="box-image-item" data-parallax-scroll="{&quot;y&quot;: 0, &quot;x&quot;: 20,  &quot;smoothness&quot;: 30 }"><img src="images/services-1-270x458.jpg" alt="" width="270" height="458"/>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="section section-lg bg-gray-100 text-center">
  <div class="container">
    <h2>Our Services</h2>
    <div class="divider-lg"></div>
    <div class="row justify-content-center">
      <div class="col-md-10 col-lg-9">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br class="d-none d-sm-inline">Vestibulum bibendum elit cursus dapibus maximus. </p>
      </div>
    </div>
    <div class="row icon-modern-list no-gutters">
      @foreach($services as $service)
      <div class="col-sm-6 col-lg-4">
        <article class="box-icon-modern modern-variant-2">
          <div class="icon-modern">
          <span><img style="border-radius: 50%;" src="/uploads/{{$service->image}}"></span>
          </div>
          <h4 class="box-icon-modern-title">{{$service->name}}</h4>
          <div class="divider"></div>
                <p><?php echo ($service->description)?></p>
        </article>
      </div>
      @endforeach
    </div>
  </div>
</section>
<section class="section section-xl bg-default">
  <div class="container">
    <div class="row no-gutters pricing-box-modern justify-content-lg-end">
      <div class="col-sm-6 col-lg-4">
        <div class="pricing-box-inner box-left">
          <h2>Schedule</h2>
          <ul class="list-md pricing-box-inner-list">
            <li>
              <h4>Monday-Friday</h4>
              <p>10am – 8pm</p>
            </li>
            <li>
              <h4>Saturday</h4>
              <p>10am – 6pm</p>
            </li>
            <li>
              <h4>Sunday</h4>
              <p>closed</p>
            </li>
          </ul>
          <p>We will be glad to see you anytime at our salon.</p><a class="button-link button-link-icon" href="/contacts">make an appointment  <span class="icon fa-arrow-right icon-primary"></span></a>
        </div>
      </div>
      <div class="d-none d-lg-block col-lg-4 img-wrap"><img src="images/home-4-6-498x688.jpg" alt="" width="498" height="688"/>
      </div>
      <div class="col-sm-6 col-lg-4">
        <div class="pricing-box-inner bg-primary context-dark box-right">
          <h2>Pricing</h2>
          <ul class="list-md pricing-box-inner-list">
            <li>
              <h4>Manicure</h4>
              <p>from $30.00</p>
            </li>
            <li>
              <h4>Pedicure</h4>
              <p>from $35.00</p>
            </li>
            <li>
              <h4>Nail Extension</h4>
              <p>from $60.00</p>
            </li>
            <li>
              <h4>Gel Pedicure</h4>
              <p>from $70.00</p>
            </li>
            <li>
              <h4>Nail Design</h4>
              <p>from $50.00</p>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="section parallax-container" data-parallax-img="images/parallax-1-1920x870.jpg">
  <div class="parallax-content section-lg context-dark text-center">
    <div class="container"> 
      <div class="row justify-content-md-center row-30"> 
        <div class="col-md-9 col-lg-8">
          <h2>Subscribe to Our Newsletter</h2>
          <div class="divider-lg"></div>
          <p class="big">Be the first to know about our promotions and discounts!</p>
        </div>
        <div class="col-md-9 col-lg-6">
          @if (count($errors) > 0)
          <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}<br></li>
              @endforeach
            </ul>
          </div>
          @endif
          @if ($message = Session::get('success'))
          <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
          </div>
          @endif
          <!-- RD Mailform-->
          <style type="text/css">
            .form-button button {
              background: #fff;
              border-color: #fff;
            }
          </style>
          <form class="rd-form-inline" method="post" action="{{url('/subscribe/send')}}">
            @csrf
            <div class="form-wrap">
              <input class="form-input" id="subscribe-form-0-email" type="email" name="email" required="" />
              <label class="form-label" for="subscribe-form-0-email">Your E-mail</label>
            </div>
            <div class="form-button">
              <button class="button button-primary" type="submit">Subscribe</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Page Footer-->
@endsection