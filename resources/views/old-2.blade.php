<section>
	<div style="margin-right: 0;background: #fafafc;" class="row">
		<div class="col-sm-5 slider_left">
			<div style="margin-right: 0;" class="row">
				<div class="col-sm-6 slider_resp"></div>
				<div class="col-sm-6 slider_resp_des">
					<div class="float-right">
				<h2>Find Best Presents & Gifts At Reasonable Price Every Time</h2>
				<p>Our online shop provides customers all over the world with fresh and interesting gift ideas for any occasion. Feel free to pick and order what you like without paying too much for it as all our products are reasonably priced and delivered free to any corner of the world.</p>
				<div class="slider_button"><a href="#">View products</a></div>
			</div>
				</div>
			</div>
			
			
		</div>

      <div class="col-sm-7 slider_part_index" style="min-height: 50px;">
        <!-- Jssor Slider Begin -->
        
        <style>
            /* jssor slider loading skin spin css */
            .jssorl-009-spin img {
                animation-name: jssorl-009-spin;
                animation-duration: 1.6s;
                animation-iteration-count: infinite;
                animation-timing-function: linear;
            }

            @keyframes jssorl-009-spin {
                from {
                    transform: rotate(0deg);
                }

                to {
                    transform: rotate(360deg);
                }
            }
        </style>
        <div id="slider1_container" class="index_slider" style="visibility: hidden; position: relative; margin: 0 auto;
        top: 0px; left: 0px; width: 1300px;max-width: 100%; height: 500px; overflow: hidden !important;">
            <!-- Loading Screen -->
            <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;max-width: 100% height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
                <img style="margin-top:-19px;position:relative;top:50%;width:38px;max-width: 100%; height:38px;" src="{{asset('/svg/loading/static-svg/spin.svg')}}" />
            </div>

            <!-- Slides Container -->
            <div data-u="slides" class="image_responsive_slider" style="position: absolute; left: 0px; top: 0px; width: 800px; height: 500px; overflow: hidden !important;">
                <div>
                    <img data-u="image" src="{{asset('/images/slider/slider5.jpg')}}" />
                </div>
                <div>
                    <img data-u="image" src="{{asset('/images/slider/slider6.jpg')}}" />
                </div>
                <div>
                    <img data-u="image" src="{{asset('/images/slider/slider1.jpg')}}" />
                </div>
            </div>

            <div data-u="arrowleft" class="jssora051" style="width:55px;height:55px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
                <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
                </svg>
            </div>
            <div data-u="arrowright" class="jssora051" style="width:55px;height:55px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
                <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
                </svg>
            </div>
            <!--#endregion Arrow Navigator Skin End -->
        </div>
        <!-- Jssor Slider End -->
    </div>
</div>
</section>