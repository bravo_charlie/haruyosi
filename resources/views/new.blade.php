@extends('layouts.new.app', ['title' => 'Home'],['discription'=> 'Flower Website Our Flower brings unique and custom Flower designs to all customers of LA area.'])

@section('content')

<!-- Swiper-->
<section class="section swiper-container swiper-slider swiper-slider-2 slider-scale-effect" data-loop="false" data-autoplay="5500" data-simulate-touch="false" data-slide-effect="fade">
  <div class="swiper-wrapper">
    @foreach($sliders as $slider)
    <div class="swiper-slide">
      <div class="slide-bg" style="background-image: url(&quot;/uploads/{{$slider->image}}&quot;)"></div>
      <div class="swiper-slide-caption section-md">
        <div class="container">
          <div class="row">
            <div class="col-sm-10 col-lg-7 col-xl-6 swiper-caption-inner">
              <h1 data-caption-animate="fadeInUp" data-caption-delay="100"><span class="text-primary">{{$slider->title}}</span><br>{{$slider->title_1}}
              </h1>
              <div class="divider-lg" data-caption-animate="fadeInLeft" data-caption-delay="550"></div>
              <p class="lead" data-caption-animate="fadeInUp" data-caption-delay="250"><?php echo ($slider->description)?></p><a class="button button-default-outline" href="/contacts" data-caption-animate="fadeInUp" data-caption-delay="450">Book Now</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    @endforeach
<!--           <div class="swiper-slide">
            <div class="slide-bg" style="background-image: url(&quot;images/slide-4-2-1920x780.jpg&quot;)"></div>
            <div class="swiper-slide-caption section-md">
              <div class="container">
                <div class="row">
                  <div class="col-sm-10 col-lg-7 col-xl-6 swiper-caption-inner">
                    <h1 data-caption-animate="fadeInUp" data-caption-delay="100">Be <span class="text-primary">Different</span> with<br>Our Flower Design
                    </h1>
                    <div class="divider-lg" data-caption-animate="fadeInLeft" data-caption-delay="550"></div>
                    <p class="lead" data-caption-animate="fadeInUp" data-caption-delay="250">Our qualified team provides a full range of Flower design services to satisfy even the most demanding clients. With our help, you can easily acquire a new look for your Flowers to stand out from the crowd.</p><a class="button button-default-outline" href="#" data-caption-animate="fadeInUp" data-caption-delay="450">Book Now</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="swiper-slide">
            <div class="slide-bg" style="background-image: url(&quot;images/slide-4-3-1920x780.jpg&quot;)"></div>
            <div class="swiper-slide-caption section-md">
              <div class="container">
                <div class="row">
                  <div class="col-sm-10 col-lg-7 col-xl-6 swiper-caption-inner">
                    <h1 class="heading-decorate" data-caption-animate="fadeInUp" data-caption-delay="100">Better <span class="text-primary">Flower Care</span><br>for Your Pleasure
                    </h1>
                    <div class="divider-lg" data-caption-animate="fadeInLeft" data-caption-delay="550"></div>
                    <p class="lead" data-caption-animate="fadeInUp" data-caption-delay="250">We are  dedicated to bringing you the best Flower care services combined with expert techniques used in the Flower industry. We are always trying to be innovative with trends, providing the best level of Flower care & design.</p><a class="button button-default-outline" href="#" data-caption-animate="fadeInUp" data-caption-delay="450">Book Now</a>
                  </div>
                </div>
              </div>
            </div>
          </div> -->

        </div>
        <!-- Swiper Pagination -->
        <div class="swiper-pagination"></div>
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
      </section>
      <section class="section section-lg bg-gray-100">
        <div class="container">
          <div class="row row-50 align-items-lg-center justify-content-xl-between">
            <div class="col-lg-6">
              <div class="box-images box-images-modern">
                <div class="box-images-item" data-parallax-scroll="{&quot;y&quot;: -10,   &quot;smoothness&quot;: 30 }"><img src="images/home-4-2-328x389.jpg" alt="" width="310" height="370"/>
                </div>
                <div class="box-images-item box-images-without-border" data-parallax-scroll="{&quot;y&quot;: 40,  &quot;smoothness&quot;: 30 }"><img src="images/home-4-1-310x370.jpg" alt="" width="328" height="389"/>
                </div>
              </div>
            </div>
            <div class="col-lg-6 col-xl-5">
              <h2 class="heading-decorate">Why Clients <br><span class="divider"></span>Choose Us
              </h2>
              <p class="big text-gray-800">Our product aims to deliver the best Flower design experience and top-notch customer service. </p>
              <p>We use all-natural, organic body products, hard-to-find polish brands and colors, iPads at every seat, and a drink menu featuring fresh-pressed juices and hand-crafted coffees.</p><a class="button button-default-outline" href="/about-us">read more</a>
            </div>
          </div>
        </div>
      </section>
      <section class="section section-lg bg-default text-center">
        <div class="container">
          <h2>Our Services</h2>
          <div class="divider-lg"></div>
          <p>We provide a wide range of services for your Flowers <br class="d-none d-sm-inline"> to look clean, attractive, and original.
          </p>
        </div>
        <div class="container">
          <div class="row no-gutters">
            <?php $count=1; ?>
            @foreach($services as $service)
            @if($count%2  == 0 )
            <div class="col-lg-4">
              <div class="box-service-modern">
                <div class="box-icon-classic box-icon-classic-vertical">
                  <div class="icon-classic-aside">
                    <div class="icon-classic">
                      <span><img src="/images/haruyosi-logo - Copy.jpg"></span>
                    </div>
                    <h4 class="icon-classic-title"><a href="/services">{{$service->name}}</a></h4>
                  </div>
                  <div class="icon-classic-body">
                    <p><?php echo ($service->description)?></p>
                  </div>
                </div>
                <div class="box-service-modern-img"><img src="/uploads/{{$service->image}}" alt="" width="390" height="312"/>
                </div>
              </div>
            </div>
            
            @else()
            <div class="col-lg-4">
              <div class="box-service-modern box-service-modern-reverse">
                <div class="box-icon-classic box-icon-classic-vertical">
                  <div class="icon-classic-aside">
                    <div class="icon-classic">
                      <span><img src="/images/haruyosi-logo - Copy.jpg"></span>
                    </div>
                    <h4 class="icon-classic-title"><a href="/services">{{$service->name}}</a></h4>
                  </div>
                  <div class="icon-classic-body">
                    <p><?php echo ($service->description)?></p>
                  </div>
                </div>
                <div class="box-service-modern-img"><img src="/uploads/{{$service->image}}" alt="" width="390" height="312"/>
                </div>
              </div>
            </div>
            @endif
            <?php $count ++; ?>
            <?php if($count > 3)
            break
            ?>
            @endforeach
          </div><a class="button button-default-outline" href="/services">View all services</a>
        </div>
      </div>
    </section>
    <section class="section parallax-container" data-parallax-img="images/parallax-2-1920x380.jpg">
      <div class="parallax-content section-sm  text-center">
        <div class="container">
          <div class="row row-30 counter-list-border">
            @foreach ($experiences as $experience)
            <div class="col-6 col-md-3">
              <!-- Box counter-->
              <article class="box-counter">
                <div class="box-counter-main">
                  <div class="counter heading-1">{{$experience->number}}</div>
                </div>
                <p class="box-counter-title">{{$experience->name}}</p>
              </article>
            </div>
            @endforeach
          </div>
        </div>
      </div>
    </section>
    <section class="section section-lg bg-default">
      <div class="container">
        <div class="row row-50">
          <div class="col-sm-6 col-lg-3 text-center text-lg-left">
            <h2>Our Staff</h2>
            <div class="divider-lg"></div>
            <p>Over the years, we have gathered a trusted, talented, and experienced team of Flower technicians and artists.</p>
            <div class="quote-with-image">
              <div class="quote-caption">
                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="88.34px" height="65.34px" viewBox="0 0 88.34 65.34" enable-background="new 0 0 88.34 65.34" overflow="scroll" xml:space="preserve" preserveAspectRatio="none">
                  <path d="M49.394,65.34v-4.131c12.318-7.088,19.924-16.074,22.811-26.965c-3.125,2.032-5.968,3.051-8.526,3.051                                                                                     c-4.265,0-7.864-1.721-10.803-5.168c-2.937-3.444-4.407-7.654-4.407-12.64c0-5.511,1.932-10.142,5.791-13.878                                                                                       C58.123,1.873,62.873,0,68.51,0c5.639,0,10.354,2.379,14.143,7.137c3.793,4.757,5.688,10.678,5.688,17.758                                                                                      c0,9.977-3.814,18.912-11.443,26.818C69.268,59.613,60.101,64.156,49.394,65.34z M0.923,65.34v-4.131                                                                                       c12.321-7.088,19.926-16.074,22.813-26.965c-3.126,2.032-5.993,3.051-8.598,3.051c-4.219,0-7.794-1.721-10.734-5.168                                                                                        C1.467,28.683,0,24.473,0,19.487C0,13.976,1.919,9.346,5.757,5.609C9.595,1.873,14.334,0,19.971,0                                                                                      c5.685,0,10.41,2.379,14.178,7.137c3.767,4.757,5.652,10.678,5.652,17.758c0,9.977-3.805,18.912-11.409,26.818                                                                                      C20.787,59.613,11.632,64.156,0.923,65.34z"></path>
                </svg>
                <h3 class="quote-text">Beautiful hands give confidence!</h3>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-9">
            <p class="text-center text-lg-right"><a class="button-link button-link-icon" href="/about-us">View All Team <span class="icon fa-arrow-right icon-primary"></span></a></p>
            <!-- Owl Carousel-->
            <div class="owl-carousel carousel-inset" data-items="1" data-lg-items="3" data-dots="true" data-nav="false" data-stage-padding="15" data-loop="false" data-margin="30" data-mouse-drag="false">
              @foreach($staffs as $staff)
              <div class="team-minimal team-minimal-type-2">
                <figure><img src="/uploads/{{$staff->image}}" alt="" width="370" height="370"></figure>
                <div class="team-minimal-caption">
                  <h4 class="team-title"><a href="/about-us#haruyosi_about">{{$staff->name}}</a></h4>
                  <p>{{$staff->designation}}</p>
                </div>
              </div>
              @endforeach
                <!-- <div class="team-minimal team-minimal-type-2">
                  <figure><img src="images/team-2-370x370.jpg" alt="" width="370" height="370"></figure>
                  <div class="team-minimal-caption">
                    <h4 class="team-title"><a href="/team-member-profile">Janette Wade</a></h4>
                    <p>Manicurist</p>
                  </div> -->
                </div>
              </div>
            </div>
          </div>
        </section>
        <section class="section section-lg bg-gray-100">
          <div class="container">
            <div class="row row-50 justify-content-center">
              <div class="col-md-6">
                <div class="box-video" data-lightgallery="group"><img src="images/home-1-9-541x369.jpg" alt="" width="541" height="369"/><a class="button-play" data-lightgallery="item" href="https://www.youtube.com/watch?v=m10Vl9TXpec"></a>
                </div>
              </div>
              <div class="col-md-6">
                <h2 class="heading-decorate"><span class="divider"></span>Our Interior</h2>
                <p class="big text-gray-800">We are located in the center of LA and our product has a minimalistic yet very spacious interior, helping us to cater all your needs.</p>
                <p>Haruyosi Flower was designed by one of the leading Californian studios with an initial idea of creating clean space for our clients to enjoy Haruyosi services.</p>
                <p>The inner space of our product includes the following:</p>
                <ul class="list-marked">
                  <li>Flower/Flowering stations</li>
                  <li>Treatment rooms</li>
                  <li>Waiting area</li>
                </ul>
              </div>
            </div>
          </div>
        </section>
        <section class="section section-xl bg-default">
          <div class="container">
            <div class="row no-gutters pricing-box-modern justify-content-lg-end">
              <div class="col-sm-6 col-lg-4">
                <div class="pricing-box-inner box-left">
                  <h2>Schedule</h2>
                  <ul class="list-md pricing-box-inner-list">
                    <li>
                      <h4>Monday-Friday</h4>
                      <p>10am – 8pm</p>
                    </li>
                    <li>
                      <h4>Saturday</h4>
                      <p>10am – 6pm</p>
                    </li>
                    <li>
                      <h4>Sunday</h4>
                      <p>closed</p>
                    </li>
                  </ul>
                  <p>We will be glad to see you anytime at our product.</p><a class="button-link button-link-icon" href="/contacts">make an appointment  <span class="icon fa-arrow-right icon-primary"></span></a>
                </div>
              </div>
              <div class="d-none d-lg-block col-lg-4 img-wrap"><img src="images/home-4-6-498x688.jpg" alt="" width="498" height="688"/>
              </div>
              <div class="col-sm-6 col-lg-4">
                <div class="pricing-box-inner bg-primary context-dark box-right">
                  <h2>Pricing</h2>
                  <ul class="list-md pricing-box-inner-list">
                    <li>
                      <h4>Flower</h4>
                      <p>from $30.00</p>
                    </li>
                    <li>
                      <h4>Flowering</h4>
                      <p>from $35.00</p>
                    </li>
                    <li>
                      <h4>Flower Extension</h4>
                      <p>from $60.00</p>
                    </li>
                    <li>
                      <h4>Gel Flowering</h4>
                      <p>from $70.00</p>
                    </li>
                    <li>
                      <h4>Flower Design</h4>
                      <p>from $50.00</p>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section class="section parallax-container" data-parallax-img="images/parallax-7-1920x1020.jpg">
          <div class="parallax-content section-lg text-center ">
            <div class="container"> 
              <h2>Testimonials</h2>
              <div class="divider-lg"></div>
              <!-- Owl Carousel-->
              <div class="owl-carousel" data-items="1" data-lg-items="3" data-dots="true" data-nav="false" data-stage-padding="15" data-loop="false" data-margin="30" data-mouse-drag="false">
                @foreach($testimonials as $testimonial)
                <div class="quote-corporate quote-corporate-center-img">
                  <div class="quote-header">
                    <h4>{{$testimonial->name}}</h4>
                    <p class="big">Client</p>
                  </div>
                  <div class="quote-body">
                    <div class="quote-text">
                      <p><?php echo ($testimonial->description)?></p>
                    </div>
                    <svg class="quote-body-mark" version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="66px" height="49px" viewbox="0 0 66 49" enable-background="new 0 0 66 49" xml:space="preserve">
                      <g></g>
                      <path d="M36.903,49v-3.098c9.203-5.315,14.885-12.055,17.042-20.222c-2.335,1.524-4.459,2.288-6.37,2.288                      c-3.186,0-5.875-1.29-8.071-3.876c-2.194-2.583-3.293-5.74-3.293-9.479c0-4.133,1.443-7.605,4.327-10.407                       C43.425,1.405,46.973,0,51.185,0c4.213,0,7.735,1.784,10.566,5.352C64.585,8.919,66,13.359,66,18.669                       c0,7.482-2.85,14.183-8.549,20.112C51.751,44.706,44.902,48.112,36.903,49z M0.69,49v-3.098                        c9.205-5.315,14.887-12.055,17.044-20.222c-2.335,1.524-4.478,2.288-6.423,2.288c-3.152,0-5.823-1.29-8.02-3.876                        C1.096,21.51,0,18.353,0,14.614c0-4.133,1.434-7.605,4.301-10.407C7.168,1.405,10.709,0,14.92,0c4.247,0,7.778,1.784,10.592,5.352                       c2.814,3.567,4.223,8.007,4.223,13.317c0,7.482-2.843,14.183-8.524,20.112C15.53,44.706,8.69,48.112,0.69,49z"></path>
                    </svg>
                  </div>
                  <div class="quote-image"><img src="/uploads/{{$testimonial->image}}" alt="" width="90" height="90"/>
                  </div>
                </div>
                @endforeach
              </div>
            </div>
          </div>
        </section>
        <section class="section section-lg bg-default text-center">
          <div class="container">
            <h2>Portfolio</h2>
            <div class="divider-lg"></div>
            <p class="block-lg">Check out the full portfolio of our works including Flower, Flowering, Flower designs, custom artworks, and more. Everything you see here was performed by our skilled manicurists and pedicurists.</p>
            <div class="row row-30">
              <!-- Isotope Filters-->
              <div class="col-lg-12">
                <div class="isotope-filters isotope-filters-horizontal">
                  <button class="isotope-filters-toggle button button-sm button-primary" data-custom-toggle="#isotope-filters" data-custom-toggle-disable-on-blur="true">Filter<span class="caret"></span></button>
                  <ul class="isotope-filters-list" id="isotope-filters">
                    <li><a class="active" data-isotope-filter="*" data-isotope-group="gallery" href="#">All</a></li>
                    @foreach($gallerycategories as $gallerycat)
                    <li><a data-isotope-filter="{{$gallerycat->id}}" data-isotope-group="gallery" href="#">{{$gallerycat->title}} </a></li>
                    @endforeach
                  </ul>
                </div>
              </div>
              <!-- Isotope Content-->
              <div class="col-lg-12">
                <div class="isotope row" data-isotope-layout="masonry" data-isotope-group="gallery" data-lightgallery="group" data-column-class=".col-sm-6.col-lg-4">
                  <?php $count=1; ?>
                  @foreach($gallerycategories as $gallcat)
                  @foreach ($gallery as $galle)
                  @if($gallcat->id === $galle->cat_id)
                  <div class="col-sm-6 col-lg-4 isotope-item" data-filter="{{$galle->cat_id}}"><a class="gallery-item" data-lightgallery="item" href="/uploads/gallery/{{$galle->file}}"><img src="/uploads/gallery/{{$galle->file}}" alt="" width="570" height="570"/></a>
                  </div>
                  @endif
                  <?php $count ++; ?>
            <?php if($count > 25)
              break
            ?>
                  @endforeach
                  @endforeach
                </div>
              </div>

            </div><a class="button button-default-outline" href="/gallery">View all portfolio</a>
          </div>
        </section>
        <section class="section-transform-bottom">
          <div class="container-fluid section-md bg-primary context-dark">
            <div style="margin-right: 0px;" class="row justify-content-center row-50">
              <div class="col-sm-10 text-center">
                <h2>Subscribe to Our Newsletter</h2>
                <div class="divider-lg"></div>
              </div>
              <div class="col-sm-10 col-lg-6">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}<br></li>
                    @endforeach
                  </ul>
                </div>
                @endif
                @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>{{ $message }}</strong>
                </div>
                @endif
                <!-- RD Mailform-->
                <form class="rd-form-inline" method="post" action="{{url('/subscribe/send')}}">
                  @csrf
                  <div class="form-wrap">
                    <input class="form-input" id="subscribe-form-0-email" type="email" name="email" required="" />
                    <label class="form-label" for="subscribe-form-0-email">Your E-mail</label>
                  </div>
                  <div class="form-button">
                    <button class="button button-primary" type="submit">Subscribe</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </section>


        @endsection