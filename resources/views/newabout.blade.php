 @extends('layouts.new.app', ['title' => 'About'],['discription'=> 'Flower Website'])

 @section('content')
 <section class="section-page-title" style="background-image: url(images/page-title-3-1920x305.jpg); background-size: cover;">
  <div class="container">
    <h1 class="page-title">Overview</h1>
  </div>
</section>
<section class="breadcrumbs-custom">
  <div class="container">
    <ul class="breadcrumbs-custom-path">
      <li><a href="/">Home</a></li>
      <li class="active">About</li>
    </ul>
  </div>
</section>
<section class="section section-lg bg-default">
  <div class="container">
    <div class="row row-50 align-items-lg-center justify-content-xl-between">
      <div class="col-lg-6">
        <div class="block-xs">
          <h2>Who We Are</h2>
          <div class="divider-lg"></div>
          <p class="big text-gray-800">Cras ut vestibulum tortor. In in nisi sit amet metus varius pulvinar in vitae ipsum nec mi sollicitudin Fusce turpis massa,</p>
          <p>In ante sapien, dapibus luctus aliquet a, accumsan sit amet dolor. Mauris id facilisis dolor. Donec malesuada, est eu dignissim eleifend, est nulla dignissim nisl. Fusce turpis massa, mattis sit.</p>
        </div>
        <div class="row row-30">
          <div class="col-md-6">
            <div class="box-contact-info-with-icon"><span class="icon mdi mdi-clock icon-primary"></span>
              <h5>Opening Hours</h5>
              <ul class="list-xs">
                <li> <span class="text-gray-800">Monday-Friday: </span> 8:00am–8:00pm
                </li>
                <li><span class="text-gray-800">Saturday:</span> 8:00am–6:00pm
                </li>
                <li><span class="text-gray-800">Sunday: </span> Closed
                </li>
              </ul>
            </div>
          </div>
          <div class="col-md-6">
            <div class="box-contact-info-with-icon"><span class="icon mdi mdi-clock icon-primary"></span>
              <h5>Our Location</h5>
              <ul class="list-xs">
                <li><span class="text-gray-800">Address: </span> Washington, USA 6036 Richmond hwy.,  VA, 2230
                </li>
                <li><span class="text-gray-800">Offices: </span> 284-290
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="box-images box-images-variant-3">
          <div class="box-images-item" data-parallax-scroll="{&quot;y&quot;: -20,   &quot;smoothness&quot;: 30 }"><img src="images/overview-1-470x282.jpg" alt="" width="470" height="282"/>
          </div>
          <div class="box-images-item box-images-without-border" data-parallax-scroll="{&quot;y&quot;: 40,  &quot;smoothness&quot;: 30 }"><img src="images/overview-2-470x282.jpg" alt="" width="470" height="282"/>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="section parallax-container" data-parallax-img="images/parallax-04-1920x1320.jpg">
  <div class="parallax-content section-lg context-dark text-center section-filter-dark">
    <div class="container">
      <h2>Flowers Can Dance </h2>
      <div class="divider-lg"></div>
      <p class="block-lg">In this video, our staff members tell about their work at Haruyosi, how they achieve the best results for their clients every day and more. Click the Play button below to watch this presentation.</p>
    </div>
    <div class="container">
      <div class="box-video-button" data-lightgallery="group"><a class="button-play" data-lightgallery="item" href="/https://www.youtube.com/watch?v=ryUxrFUk6MY"><span class="icon fa-play"></span></a></div>
    </div>
  </div>
</section>
<section id="haruyosi_about" class="section section-md bg-default text-center">
  <div class="container">
    <h2>Our Professional Team</h2>
    <div class="divider-lg"></div>
    <p class="block-lg">Lorem ipsum dolor sit amet, consectetur adipiscing elit.  Vestibulum bibendum elit cursus dapibus maximus. Maecenas sapien urna, cursus ut turpis non, gravida vehicula nisl. </p>
    <div class="row row-30">
      <div class="col-12">
        <!-- Owl Carousel-->
        <div class="owl-carousel carousel-creative" data-items="1" data-lg-items="3" data-dots="true" data-nav="false" data-stage-padding="15" data-loop="false" data-margin="30" data-mouse-drag="false">
          @foreach($staffs as $staff)
          <div class="team-minimal team-minimal-with-shadow">
            <figure><a href="" data-toggle="modal" data-target="#myModal{{$staff->id}}"><img src="/uploads/{{$staff->image}}" alt="" width="370" height="370"></a></figure>
            <div class="team-minimal-caption">
              <h4 class="team-title"><a href="#" data-toggle="modal" data-target="#myModal{{$staff->id}}">Samantha York </a></h4>
              <p>{{$staff->designation}}</p>
            </div>
          </div>
          @endforeach

        </div>

        @foreach($staffs as $staff)
        <!-- Modal -->
        <div class="modal fade" id="myModal{{$staff->id}}" role="dialog">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body">
                <section class="section bg-default">
                  <div class="container">
                    <div class="row row-50">
                      <div class="col-lg-6 col-xl-4 text-center">
                        <div class="row row-50">
                          <div class="col-12"><img class="img-shadow" src="/uploads/{{$staff->image}}" alt="" width="370" height="370"/>
                          </div>
                          <div style="margin-bottom: 20px;" class="col-12"><a style="padding: 10px 20px;" class="button button-primary" href="mailto:{{$staff->mail}}">Mail Me</a></div>
                          <div class="col-12"><a style="padding: 10px 15px;" class="button button-primary" href="/contacts">Make an appointment</a></div>
                          <div class="col-12">
                            <ul class="list-inline social-list">
                              <li><a class="icon icon-circle icon-circle-sm icon-circle-gray fa-facebook" target="_blank" href="{{$staff->facebook_link}}"></a></li>
                              <li><a class="icon icon-circle icon-circle-sm icon-circle-gray fa-twitter" target="_blank" href="{{$staff->twitter_link}}"></a></li>
                              <li><a class="icon icon-circle icon-circle-sm icon-circle-gray fa-instagram" target="_blank" href="{{$staff->instagram_link}}"></a></li>
                            </ul>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-6 col-xl-8">
                        <div class="box-team-info">
                          <div class="box-team-info-header">
                            <h3>{{$staff->name}}</h3>
                            <p>{{$staff->designation}}</p>
                            <div class="divider-lg"></div>
                          </div>
                          <p><?php echo ($staff->description)?></p>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
              </div>
              <div style="padding: 30px;" class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
        @endforeach
      </div>
      <!-- <div class="col-12"><a class="button button-default-outline" href="/our-team">View all team</a></div> -->
    </div>
  </div>
</section>
<section class="section parallax-container" data-parallax-img="images/parallax-1-1920x870.jpg">
  <div class="parallax-content section-lg context-dark text-center">
    <div class="container"> 
      <div class="row justify-content-md-center row-30"> 
        <div class="col-md-9 col-lg-8">
          <h2>Subscribe to Our Newsletter</h2>
          <div class="divider-lg"></div>
          <p class="big">Be the first to know about our promotions and discounts!</p>
        </div>
        <div class="col-md-9 col-lg-6">
          @if (count($errors) > 0)
          <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}<br></li>
              @endforeach
            </ul>
          </div>
          @endif
          @if ($message = Session::get('success'))
          <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
          </div>
          @endif
          <!-- RD Mailform-->
          <style type="text/css">
            .form-button button {
              background: #fff;
              border-color: #fff;
            }
          </style>
          <form class="rd-form-inline" method="post" action="{{url('/subscribe/send')}}">
            @csrf
            <div class="form-wrap">
              <input class="form-input" id="subscribe-form-0-email" type="email" name="email" required="" />
              <label class="form-label" for="subscribe-form-0-email">Your E-mail</label>
            </div>
            <div class="form-button">
              <button class="button button-primary" type="submit">Subscribe</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection