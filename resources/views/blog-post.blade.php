 @extends('layouts.new.app', ['title' => 'Blog Post'],['discription'=> 'Flower Website Our Flower brings unique and custom Flower designs to all customers of LA area.'])

 @section('content')
       <style type="text/css">
                  .gallery-item{
                    height: auto;
                  }
                </style>
 <section class="section-page-title" style="background-image: url(/images/page-title-5-1920x305.jpg); background-size: cover;">
  <div class="container">
    <h1 class="page-title">Single Post</h1>
  </div>
</section>
<section class="breadcrumbs-custom">
  <div class="container">
    <ul class="breadcrumbs-custom-path">
      <li><a href="/">Home</a></li>
      <li><a href="/blogs">Blog</a></li>
      <li class="active">Single Post</li>
    </ul>
  </div>
</section>
<section class="section section-lg bg-default">
  <div class="container">
    <div class="row row-50">
      <div class="col-lg-8">
        <div class="single-post-content">
          <h4>Holiday Haircuts and Styling: Easy Tutorials for Everyone</h4>
          <ul class="post-meta-list">
            <li class="time">{{ $blogs->created_at->format('d M , Y') }}</li>
            <li>by Admin</li>
          </ul><img src="/uploads/{{$blogs-> f_image}}" alt="{{$blogs->name}}" width="735" height="404"/>
          <p><?php echo ($blogs -> description)?></p>
          <img src="/uploads/{{$blogs-> i_image}}" alt="{{$blogs->name}}" width="735" height="404"/>

          <!-- <div class="single-post-share-block">
            <p class="big text-gray-800">Share:</p>
            <ul class="list-inline social-list">
              <li><a class="icon icon-circle icon-circle-sm icon-circle-gray fa-facebook" href="#"></a></li>
              <li><a class="icon icon-circle icon-circle-sm icon-circle-gray fa-twitter" href="#"></a></li>
              <li><a class="icon icon-circle icon-circle-sm icon-circle-gray fa-google-plus" href="#"></a></li>
            </ul>
          </div> -->
          
        </div>
        <!-- <div class="single-post-author">
          <div class="single-post-author-img"><img src="/images/user-5-115x115.jpg" alt="" width="115" height="115"/>
            <h5>Mary Lucas </h5>
          </div>
          <div class="single-post-author-content">
            <h5>About the Author</h5>
            <p>Donec posuere pellentesque accumsan. Quisque cursus in ex id mollis. Suspendisse potenti. Praesent fermentum fringilla erat ac porta. Praesent congue justo efficitur libero dapibus, et eleifend tellus. Fermentum fringilla erat ac porta. Praesent congue justo efficitur.</p>
          </div>
        </div> -->
              <!-- <div class="post-comment-block">
                <h4>Comments</h4>
                <div class="post-comment-item">
                  <div class="post-comment-item-img"><img src="images/user-4-90x90.jpg" alt="" width="90" height="90"/>
                  </div>
                  <div class="post-comment-item-content">
                    <div class="post-comment-item-header">
                      <ul class="list-inline">
                        <li><a href="#">Janice Brown</a></li>
                        <li>June 20, 2018 at 10:15am</li>
                      </ul>
                    </div>
                    <div class="post-comment-item-text">
                      <p>Thanks to the author for such a useful article. It is really important for me to be informed about beauty news.</p>
                    </div>
                    <div class="post-comment-item-footer"><a href="#"><span class="icon mdi mdi-reply"> </span>Reply</a></div>
                  </div>
                </div>
                <div class="post-comment-item post-comment-item-reply">
                  <div class="post-comment-item-img"><img src="images/user-3-90x90.jpg" alt="" width="90" height="90"/>
                  </div>
                  <div class="post-comment-item-content">
                    <div class="post-comment-item-header">
                      <ul class="list-inline">
                        <li><a href="#">Betty Riley</a></li>
                        <li>June 20, 2018 at 10:30am</li>
                      </ul>
                    </div>
                    <div class="post-comment-item-text">
                      <p>Thank you!</p>
                    </div>
                    <div class="post-comment-item-footer"><a href="#"><span class="icon mdi mdi-reply"></span>Reply</a></div>
                  </div>
                </div>
              </div> -->
<!--               <div class="form-comment">
                <h4>Leave a Comment</h4>
                <form class="rd-mailform text-left rd-form" data-form-output="form-output-global" data-form-type="contact" method="post" action="bat/rd-mailform.php">
                  <div class="row row-15">
                    <div class="col-sm-6">
                      <div class="form-wrap">
                        <label class="form-label" for="contact-name">First name</label>
                        <input class="form-input" id="contact-name" type="text" name="name" data-constraints="@Required">
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-wrap">
                        <label class="form-label" for="contact-sec-name">Last name</label>
                        <input class="form-input" id="contact-sec-name" type="text" name="sec-name" data-constraints="@Required">
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-wrap">
                        <label class="form-label" for="contact-phone">Phone</label>
                        <input class="form-input" id="contact-phone" type="text" name="phone" data-constraints="@Numeric @Required">
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-wrap">
                        <label class="form-label" for="contact-email">E-Mail</label>
                        <input class="form-input" id="contact-email" type="email" name="email" data-constraints="@Email @Required">
                      </div>
                    </div>
                    <div class="col-12">
                      <div class="form-wrap">
                        <label class="form-label" for="contact-message">Message</label>
                        <textarea class="form-input" id="contact-message" name="message" data-constraints="@Required"></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="form-button group-sm text-left">
                    <button class="button button-primary" type="submit">Submit</button>
                  </div>
                </form>
              </div> -->
            </div>
            <div class="col-lg-4">
              <div class="blog-aside-list">
                <div class="blog-aside-list-item">
                  <h4 class="blog-aside-list-item-title">Categories</h4>
                  <ul class="list-marked">
                    <li><a href="/blogs">All</a></li>
                    @foreach($blogcategories as $blogcategory)
                    <li><a href="/blogs/category/{{$blogcategory->id}}">{{$blogcategory->title}}</a></li>
                    @endforeach
                  </ul>
                </div>
<!--                 <div class="blog-aside-list-item">
                  <h4 class="blog-aside-list-item-title">Search</h4>
                  <form class="rd-search" action="search-results" method="GET" data-search-live="rd-search-results-live">
                    <div class="form-wrap">
                      <label class="form-label" for="rd-search-form-input">Search...</label>
                      <input class="form-input" id="rd-search-form-input" type="text" name="s" autocomplete="off">
                      <button class="icon mdi mdi-magnify" type="submit"></button>
                    </div>
                  </form>
                </div> -->
               <!--  <div class="blog-aside-list-item">
                  <h4 class="blog-aside-list-item-title">Archive</h4>
                  <ul class="list-marked list-two-column">
                    <li><a href="#">Jun 2018</a></li>
                    <li><a href="#">Jul 2018</a></li>
                    <li><a href="#">Aug 2018</a></li>
                    <li><a href="#">Sep 2018</a></li>
                    <li><a href="#">Oct 2018</a></li>
                    <li><a href="#">Nov 2018</a></li>
                    <li><a href="#">Dec 2018</a></li>
                    <li><a href="#">Jan 2018</a></li>
                    <li><a href="#">Feb 2018</a></li>
                    <li><a href="#">Mar 2018</a></li>
                  </ul>
                </div> -->
                 <div class="blog-aside-list-item">
                  <h4 class="blog-aside-list-item-title">Gallery</h4>
                  <div class="blog-aside-gallery" data-lightgallery="group">
                    <?php $count=1; ?>
                    @foreach($gallery as $galley)
                    <?php 
                    $string = $galley->image;
                    $string = substr(strrchr($string, '.'), 1);
                ?>
                @if($string == "jpg" || $string == "png" || $string == "jpeg")
                    <div class="blog-aside-gallery-item"><a class="gallery-item" href="/uploads/gallery/{{$galley->image}}" data-lightgallery="item"><img src="/uploads/gallery/{{$galley->image}}" alt="" width="180" height="140"/></a></div>
                    @endif
                    <?php $count ++; ?>
                    <?php if($count > 6)
                    break
                    ?>
                    @endforeach
                  </div>
                </div>
                <div class="blog-aside-list-item">
                  <h4 class="blog-aside-list-item-title">Recent Blog Posts</h4>
                  <?php $count=1; ?>
                  @if(count($allblogs))
                  @foreach($allblogs as $blog)
                  <div class="blog-aside-post">
                    <h5><a href="/blog-detail/{{$blog->id}}">{{$blog->title}}</a></h5>
                    <p>{{ $blog->created_at->format('D M , Y | H:i') }}</p>
                  </div>
                  <?php $count ++; ?>
                  <?php if($count > 3)
                  break
                  ?>
                  @endforeach
                  @else
                  <h5>There are no posts in Blog.</h5>
                  @endif
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Page Footer-->
      @endsection