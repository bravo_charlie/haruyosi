@extends('layouts.main')

@section('content')
<div class="breadcrumbs">
	<div class="container">
		<div class="row">
			<div class="breadcrumbs__title">
				<h5 class="page-title">About Us</h5>
			</div>
			<div class="breadcrumbs__items">
				<div class="breadcrumbs__content">
					<div class="breadcrumbs__wrap">
						<div class="breadcrumbs__item">
							<a href="/" class="breadcrumbs__item-link is-home" rel="home" title="Home">Home</a></div>
							<div class="breadcrumbs__item">
								<div class="breadcrumbs__item-sep">/</div>
							</div>
							<div class="breadcrumbs__item"><span class="breadcrumbs__item-target">About Us</span></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<section style="padding: 20px 0;" class="overview_section">
	<div style="max-width: 63.5%;border:none; padding: 0;" class="container company_overview">
		<div>
		<h1>A FEW WORDS ABOUT US</h1>
		<p>who we are</p>
	</div>
	<div class="row company_about_desc">
		<div class="col-sm-4 about_def_res">
			<p>Since 2005 Gift Box has been putting the magic and excitement back into gifts and homeware, delivering stylish presents and home decorations – all at irresistible prices. Original, gorgeous, and perfect for any occasion, our wide range of gifts and home accessories is either exclusively designed or lovingly chosen by our talented in-house design and buying teams.</p>
		</div>
		<div class="col-sm-4 about_def_res">
			<p>Our unique approach to helping you choose the right present for your loved ones along with free delivery and packaging makes us No. 1 provider of gifts and presents for your family, friends, and colleagues. We will always go the extra mile to ensure you are happy with your purchase. So if you are looking for great and creative gifts that are not overpriced, we will be glad to help you!</p>
		</div>
		<div class="col-sm-4 about_def_res">
			<div class="background_about_small">
				<h3>Our products</h3>
				<p style="text-align:center !important;">The gifts available at our store are 100% healthy for you.</p>
			</div>

		</div>
	</div>
	<div style="padding-top: 50px;" class="container counter_text">
		<div class="row text-center">
	        <div class="col">
	        <div class="counter">
      <h2 class="timer count-title count-number" data-to="35" data-speed="1700"></h2>
       <p class="count-text ">Offline stores</p>
    </div>
	        </div>
              <div class="col">
               <div class="counter">
      <h2 class="timer count-title count-number" data-to="17" data-speed="1700"></h2>
      <p class="count-text ">Gift Collections</p>
    </div>
              </div>
              <div class="col">
                  <div class="counter">
      <h2 class="timer count-title count-number" data-to="450" data-speed="1700"></h2>
      <p class="count-text ">Products</p>
    </div></div>
              <div class="col">
              <div class="counter">
      <h2 class="timer count-title count-number" data-to="93" data-speed="1700"></h2>
      <p class="count-text ">Team members</p>
    </div>
              </div>
         </div>
</div>
	</div>
<div class="special_functions">
	<div class="container">
	<div class="row">
		<div class="col-sm-4 exclusive_elements">
			<img src="https://ld-wp.template-help.com/woocommerce_prod-16359/v2/wp-content/uploads/2017/09/icon01.png">
			<h3 style="color: #000000;">FREE SHIPPING</h3>
			<P style="text-align: center !important;padding: 5px 0;" >We offer free shipping all over the USA and Canada to orders over $100. Overseas delivery is paid.</P>
		</div>
		<div class="col-sm-4 exclusive_elements">
			<img src="https://ld-wp.template-help.com/woocommerce_prod-16359/v2/wp-content/uploads/2017/09/icon02.png">
			<h3 style="color: #000000;">24/7 SUPPORT</h3>
			<P style="text-align: center !important;padding: 5px 0;" >If you have any questions regarding your order, payment or delivery, please contact our Customer Support any time.</P>
		</div>
		<div class="col-sm-4 exclusive_elements">
			<img src="https://ld-wp.template-help.com/woocommerce_prod-16359/v2/wp-content/uploads/2017/09/icon03.png">
			<h3 style="color: #000000;">PAYMENT PROCESS</h3>
			<P style="text-align: center !important;padding: 5px 0;" >We work with prepayments. We can ship the order only after it was paid. We take credit cards, checks and Paypal payments.</P>
		</div>
	</div>
</div>
	</div>

</section>
<section class="product_shop">
	<div style="max-width: 65.5%;" class="container product_container_res">
		<div class="row">
			<div class="col-sm-4">
				<div class="banner_hover">
				<a href="#"><img src="/images/banner-3.png"></a>
			</div>
			</div>
			<div class="col-sm-4">
				<div class="banner_hover">
				<a href=""><img src="/images/banner-2.png"></a>
			</div>
			</div>
			<div class="col-sm-4">
				<div class="banner_hover">
				<a href=""><img src="/images/banner-1.png"></a>
			</div>
			</div>
		</div>
	</div>
</section>
<section class="top_products">
	<div style="max-width: 68.5%;border:none;" class="container company_overview product_container_res">
		<div>
		<h1>TOP PRODUCTS</h1>
		<p>our top-rated gift ideas</p>
	</div>
	<div class="row">
		<div class="col-sm-3">
			<div class="product_items">
				<div class="product_items_image">
					<a href="#"><img src="/images/product1.jpg"></a>
				</div>
					<div class="product_items_details">
						<h3><a href="#">Petite Garnet Necklace Sterling Silver</a></h3>
						<p style="text-align:left;">Sterling silver large hoop style earrings. marked 925 Thailand. great condition.</p>
				</div>
				<div class="slider_button"><a href="#">Get Quotation</a></div>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="product_items">
				<div class="product_items_image">
					<a href="#"><img src="/images/product4.jpg"></a>
				</div>
					<div class="product_items_details">
						<h3><a href="#">Arwen spinner ring, spinning ring</a></h3>
						<p style="text-align:left;">Sterling silver large hoop style earrings. marked 925 Thailand. great condition.</p>
				</div>
				<div class="slider_button"><a href="#">Get Quotation</a></div>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="product_items">
				<div class="product_items_image">
					<a href="#"><img src="/images/product2.jpg"></a>
				</div>
					<div class="product_items_details">
						<h3><a href="#">Raindrops Boho bag, Crossbody bag</a></h3>
						<p style="text-align:left;">Sterling silver large hoop style earrings. marked 925 Thailand. great condition.</p>
				</div>
				<div class="slider_button"><a href="#">Get Quotation</a></div>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="product_items">
				<div class="product_items_image">
					<a href="#"><img src="/images/product3.jpg"></a>
				</div>
					<div class="product_items_details">
						<h3><a href="#">Simulated Aquamarine</a></h3>
						<p style="text-align:left;">Sterling silver large hoop style earrings. marked 925 Thailand. great condition.</p>
				</div>
				<div class="slider_button"><a href="#">Get Quotation</a></div>
			</div>
		</div>
	</div>
</div>
</section>
<section class="top_blogs">
	<div style="max-width: 65.5%;border:none;padding: 0;" class="container company_overview">
		<div>
		<h1>LATEST FROM THE BLOG</h1>
		<p>news, events, and tips from our team</p>
	</div>
	<div class="row">
		<div class="col-sm-6">
		<div class="main_image_blog">
			<a href="#"><img src="/images/blog1.jpg"></a>
		</div>
		<div class="main_text_blog">
			<h6><a href="#">How to choose the right gifts?</a></h6>
		<p>One of the most wide-spread traditions in this world is gift-giving. This is an especially hot subject during the holiday season, but is stays highly relevant throughout the entire year. Birthdays, weddings, births, anniversaries, new…</p>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="main_image_blog">
			<a href="#"><img src="/images/blog2.jpg"></a>
		</div>
		<div class="main_text_blog">
			<h6><a href="#">Accessories that match your daily style</a></h6>
		<p>One of the most wide-spread traditions in this world is gift-giving. This is an especially hot subject during the holiday season, but is stays highly relevant throughout the entire year. Birthdays, weddings, births, anniversaries, new…</p>
		</div>
	</div>
</div>
	</div>
</section>
<section>
  <div class="testimonial">
    <div style="max-width: 65.5%;border:none;padding: 0;" class="container testimonial_res_cont">
      <div class="row">
        <div class="col-sm-5 testimonial_side">
          <img style="max-width: 100%;" src="/images/testimonial_image.jpg" alt="testimonial">
        </div>
        <div class="col-sm-1"></div>
        <div class="col-sm-6 testimonial_side">
          <div class="space_responsive make_space"></div>
          <div class="sqs-block-content"><h2 style="white-space: pre-wrap;color: #b8958a;">Kind Words</h2></div>
               <div id="myCarousel" class="carousel slide" data-ride="carousel">   
        <!-- Wrapper for carousel items -->
        <div class="carousel-inner">
          <div class="item carousel-item active">
                <div class="testimonial_part">
                  <p>— LP - WAKEFIELD, DECEMBER 2018</p>
                  <h3>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante.'</h3>
                </div>
          </div>
          <div class="item carousel-item">
                <div class="testimonial_part">
                  <p>— TW - NELSON, SEPTEMBER 2018</p>
                  <h3>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante.'</h3>
            </div>      
          </div>
          <div class="item carousel-item">
                <div class="testimonial_part">
                  <p>— JB - TIMARU, AUGUST 2018</p>
                  <h3>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante.'</h3>
            </div>      
          </div>
        </div>
        <!-- Carousel controls -->
        <a class="carousel-control left carousel-control-prev" href="#myCarousel" data-slide="prev">
          <i class="fa fa-chevron-left"></i>
        </a>
        <a class="carousel-control right carousel-control-next" href="#myCarousel" data-slide="next">
          <i class="fa fa-chevron-right"></i>
        </a>
      </div>
    </div>
  </div>
</div>
</div>
</section>
@endsection