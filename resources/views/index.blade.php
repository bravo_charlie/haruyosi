@extends('layouts.main')

@section('content')

<section>
      <div style="min-height: 50px;">
        <!-- Jssor Slider Begin -->
        
        <style>
            /* jssor slider loading skin spin css */
            .jssorl-009-spin img {
                animation-name: jssorl-009-spin;
                animation-duration: 1.6s;
                animation-iteration-count: infinite;
                animation-timing-function: linear;
            }

            @keyframes jssorl-009-spin {
                from {
                    transform: rotate(0deg);
                }

                to {
                    transform: rotate(360deg);
                }
            }
        </style>
        <div id="slider1_container" style="visibility: hidden; position: relative; margin: 0 auto;
        top: 0px; left: 0px; width: 1300px; height: 500px; overflow: hidden;">
            <!-- Loading Screen -->
            <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
                <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="{{asset('/svg/loading/static-svg/spin.svg')}}" />
            </div>

            <!-- Slides Container -->
            <div data-u="slides" style="position: absolute; left: 0px; top: 0px; width: 1300px; height: 500px; overflow: hidden;">
                <div>
                    <img data-u="image" src="{{asset('/images/slider/slider1.jpg')}}" />
                    <div class="section-sm context-dark text-center">
          <div class="container">
            <div class="row justify-content-center">
              <div class="col-md-10">
                <div class="pb-5">
                  <div class="box-typography"><span class="box-typography-inner heading-4">Welcome to</span><span class="box-typography-inner"><span class="text-uppercase">Haruyosi</span></span></div>
                  <p class="text-white">A perfect view for both <br class="d-none d-lg-inline-block">usable and creative designs.</p><a class="button button-primary button-sm slider_button" href="#">See Haruyosi</a>
                </div>
              </div>
            </div>
          </div>
        </div>
                    <!-- <span class="slider_text"><h2>Meaningful Gifts for Magical Moments</h2><a href="#" class="btn-slider">SHOP NOW -></a></span> -->
                </div>
                <div>
                    <img data-u="image" src="{{asset('/images/slider/slider2.jpg')}}" />
                </div>
                <div>
                    <img data-u="image" src="{{asset('/images/slider/slider3.jpg')}}" />
                </div>
            </div>
            
            <!--#endregion Bullet Navigator Skin End -->

            <div data-u="arrowleft" class="jssora051" style="width:55px;height:55px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
                <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
                </svg>
            </div>
            <div data-u="arrowright" class="jssora051" style="width:55px;height:55px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
                <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
                </svg>
            </div>
            <!--#endregion Arrow Navigator Skin End -->
        </div>
        <!-- Jssor Slider End -->
    </div>
</section>
<section class="product_shop">
	<div style="max-width: 65.5%;" class="container product_container_res">
		<div class="row">
			<div class="col-sm-4">
				<div class="banner_hover">
				<a href="#"><img src="/images/banner-3.png"></a>
			</div>
			</div>
			<div class="col-sm-4">
				<div class="banner_hover">
				<a href=""><img src="/images/banner-2.png"></a>
			</div>
			</div>
			<div class="col-sm-4">
				<div class="banner_hover">
				<a href=""><img src="/images/banner-1.png"></a>
			</div>
			</div>
		</div>
	</div>
	<div class="product_shop"></div>
</section>

<section class="about_part_a">
	<div style="max-width: 65%;" class="container cont_responsive">
		<div class="row">
			<div style="padding-right: 0;" class="col-sm-6 mob_res_about">
				<div class="about_desc special-gift">
					<h2>WE SPECIALIZE IN<br>ALL KINDS OF GIFTS</h2>
					<p>Since our establishment in 2005, we have been offering a wide variety of gifts to customers worldwide. Our team specializes in picking the best presents for everyone. Even if you are looking for something very special, our consultants and managers will be glad to help you choose just the thing you need for that special person in your life. Rest assured that we have lots of creative gift ideas to surprise you as well as the person you love!</p>
					<div class="slider_button"><a href="#">View products</a></div>
				</div>
			</div>
			<div class="col-sm-6 mob_res_about_image">
				<div class="about_image1">
					<a style="text-decoration: none;" href="#">
				<div class="about_image_text">
						<div class="text_color_box">
						<p>Send love to your<br>loved ones with our</p>
						<span class="desc_about">GIFT IDEAS <span class="lnr lnr-arrow-right"></span></span>
						</div>
				</div>
					</a>
				</div>
			</div>
			<div class="col-sm-6 mob_res_about">
				<div class="about_image2 modify_about">
					<a style="text-decoration: none;" href="#">
				<div class="about_image_text">
						<div class="text_color_box">
						<p>Send love to your<br>loved ones with our</p>
						<span class="desc_about">GIFT IDEAS <span class="lnr lnr-arrow-right"></span></span>
						</div>
				</div>
					</a>
				</div>
			</div>
			<div style="padding-right: 0;margin-top: 24px;" class="col-sm-6 responsive_gifts">
				<div class="about_desc special-gift">
					<h2>WE SPECIALIZE IN<br>ALL KINDS OF GIFTS</h2>
					<p>Since our establishment in 2005, we have been offering a wide variety of gifts to customers worldwide. Our team specializes in picking the best presents for everyone. Even if you are looking for something very special, our consultants and managers will be glad to help you choose just the thing you need for that special person in your life. Rest assured that we have lots of creative gift ideas to surprise you as well as the person you love!</p>
					<div class="slider_button"><a href="#">View products</a></div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="packet_part">
	<div style="max-width: 63.5%;" class="container image_packet">
		<div class="row">
			<div class="col-sm-7">
				<div style="border:none;" class="about_desc special-gift">
					<h2>WE PACK AND DELIVER<br>GIFTS FOR FREE</h2>
					<p>All gifts we sell are provided with free packaging, which you can choose and customize online. We will also organize a free delivery of any chosen gift to a preferred location as we provide free worldwide delivery.</p>
					<div class="slider_button"><a href="#">View products</a></div>
				</div>
			</div>
			<div class="col-sm-5"></div>
		</div>
	</div>
</section>
<section class="overview_section">
	<div style="max-width: 63.5%;" class="container company_overview">
		<div>
		<h1>COMPANY OVERVIEW</h1>
		<p>about our company</p>
	</div>
	<div class="row company_overview_desc">
		<div class="col-sm-6 company_desc">
			<p>Since 2005 Gift Box has been putting the magic and excitement back into gifts and homeware, delivering stylish presents and home decorations – all at irresistible prices. Original, gorgeous, and perfect for any occasion, our wide range of gifts and home accessories is either exclusively designed or lovingly chosen by our talented in-house design and buying teams.</p>
		</div>
		<div class="col-sm-6 company_desc">
			<p>Our unique approach to helping you choose the right present for your loved ones along with free delivery and packaging makes us No. 1 provider of gifts and presents for your family, friends, and colleagues. We will always go the extra mile to ensure you are happy with your purchase. So if you are looking for great and creative gifts that are not overpriced, we will be glad to help you!</p>
		</div>
		<div class="col-sm-4 exclusive_elements">
			<span class="lnr lnr-gift gift_icon"></span>
			<h3>EXCLUSIVE GIFTS</h3>
			<P style="text-align: center !important;padding: 5px 0;" >Our catalog includes a lot of exclusive gifts available at affordable prices.</P>
		</div>
		<div class="col-sm-4 exclusive_elements">
			<span class="lnr lnr-text-align-right gift_icon"></span>
			<h3>Holiday Decorations</h3>
			<P style="text-align: center !important;padding: 5px 0;" >We are glad to offer a wide choice of decorations for any holiday.</P>
		</div>
		<div class="col-sm-4 exclusive_elements">
			<span class="lnr lnr-magic-wand gift_icon"></span>
			<h3>Amazing Accessories</h3>
			<P style="text-align: center !important;padding: 5px 0;" >Our online shop has lots of amazing accessories available for purchase.</P>
		</div>
	</div>
	<div style="max-width: 90%;" class="container counter_text">
		<div class="row text-center">
	        <div class="col">
	        <div class="counter">
      <h2 class="timer count-title count-number" data-to="35" data-speed="1700"></h2>
       <p class="count-text ">Offline stores</p>
    </div>
	        </div>
              <div class="col">
               <div class="counter">
      <h2 class="timer count-title count-number" data-to="17" data-speed="1700"></h2>
      <p class="count-text ">Gift Collections</p>
    </div>
              </div>
              <div class="col">
                  <div class="counter">
      <h2 class="timer count-title count-number" data-to="450" data-speed="1700"></h2>
      <p class="count-text ">Products</p>
    </div></div>
              <div class="col">
              <div class="counter">
      <h2 class="timer count-title count-number" data-to="93" data-speed="1700"></h2>
      <p class="count-text ">Team members</p>
    </div>
              </div>
         </div>
<div style="text-align: center;" class="slider_button"><a href="#">View products</a></div>
</div>
	</div>
</section>
<section class="special-gift">
	<div class="container">
	<div style="margin-right: 0;" class="row">
		<div class="col-sm-4"><div class="gift_part1">
			<h2>SPECIAL GIFT COLLECTION</h2>		
			<span class="button-text"><a href="">Browse now <span class="lnr lnr-arrow-right"></span></a></span>
		</div>
		</div>
		<div class="col-sm-4"><div class="gift_part2">
			<h2>LIMITED GIFT EDITION</h2>		
			<span class="button-text"><a href="">Browse now <span class="lnr lnr-arrow-right"></span></a></span>
		</div>
		</div>
		<div class="col-sm-4"><div class="gift_part3">
			<h2>GUARANTEED HITS NOW</h2>		
			<span class="button-text"><a href="">Browse now <span class="lnr lnr-arrow-right"></span></a></span>
		</div>
		</div>
	</div>
</div>
</section>

@endsection