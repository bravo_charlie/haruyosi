@extends('layouts.dashboard.app')

@section('content')
<!-- PAGE CONTAINER-->
<div class="col-lg-12">
									<div class="container card">
										<div class="card-header">
											<div class="au-breadcrumb-left">
												<span class="au-breadcrumb-span">You are here:</span>
												<ul class="list-unstyled list-inline au-breadcrumb__list">
													<li class="list-inline-item">
														<a href="/home">Home</a>
													</li>
													<li class="list-inline-item seprate">
														<span>/</span>
													</li>
													<li class="list-inline-item active">
														<a href="/home/gallery">gallery</a>
													</li>
												</ul>
											</div>
										</div>
<div class="row m-t-30">
	<div class="col-md-12">
	<a style="margin-bottom: 10px;" href="/home/gallery/create" class="btn btn-primary">Add New Image</a>
	<!-- <a style="margin-bottom: 10px;" href="/home/gallery/createVideo" class="btn btn-primary">Add New Video</a> -->
		<!-- DATA TABLE-->
		<div class="table-responsive m-b-40">
			<table class="table table-borderless table-data3">
				<thead>
					<tr>
						<th>Id</th>
						<th>Name</th>
						<th>Category</th>
						<th>Image</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($gallery as $galler)
					<tr>
						<td>{{ $loop->iteration }}</td>
						<td>{{$galler->name}}</td>
						<td>
							@foreach($gallerycategories as $Postcat)
							@if($Postcat->id == $galler->cat_id)
							{{$Postcat->title}}
							@endif
							@endforeach
						</td>
						<?php 
                    $string = $galler->file;
                    $string = substr(strrchr($string, '.'), 1);
                ?>
                @if($string == "jpg" || $string == "png" || $string == "jpeg")
						<td class="process"><img src="/uploads/gallery/{{$galler->file}}"></td>
						@else
						<td class="process"><img src="/uploads/gallery/{{$galler->file}}"></td>
						@endif
						<td class="make_btn_straight"><a href="{{route('gallery.edit', $galler->id)}}"><button class="btn btn-primary make-btn">Edit</button></a>|
				<form method="post" action="{{route('gallery.delete',$galler->id)}}">
					@csrf
					{{ method_field('DELETE') }}
					<button type="submit" onclick="makeWarning(event)" class="btn btn-danger">Delete</button>
				</form>
				</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<div class="col-12">
                  <ul class="pagination">
                    <li style="margin: 0 auto;" class="page-item">{{ $gallery->links() }}</li>
                  </ul>
                </div>
		<!-- END DATA TABLE-->
	</div>
</div>
</div>
</div>
</div>
<script type="text/javascript">
	function makeWarning(evt){
		let result = confirm("Are you sure to Delete?");
			if(! result){
				evt.stopPropagation();
				evt.preventDefault();	
			}
	}
</script>

@endsection