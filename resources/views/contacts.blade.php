 @extends('layouts.new.app', ['title' => 'Contact'],['discription'=> 'Flower Website contact'])

 @section('content')
 <section class="section-page-title" style="background-image: url(images/page-title-4-1920x305.jpg); background-size: cover;">
  <div class="container">
    <h1 class="page-title">Contacts</h1>
  </div>
</section>
<section class="breadcrumbs-custom">
  <div class="container">
    <ul class="breadcrumbs-custom-path">
      <li><a href="/">Home</a></li>
      <li class="active">Contacts</li>
    </ul>
  </div>
</section>
<!-- Mailform-->
<section class="section section-md">
  <div class="container">
    <div class="row row-50">
      <div class="col-lg-8">
        <h2>Contact us</h2>
        <div class="divider-lg"></div>
        <p>You can contact us any way that is convenient for you. We are available 24/7 via fax or email. <br class="d-none d-lg-inline">You can also use a quick contact form below or visit our salon personally.</p>
@if (count($errors) > 0)
          <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}<br></li>
              @endforeach
            </ul>
          </div>
          @endif
          @if ($message = Session::get('success'))
          <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
          </div>
          @endif
      <!-- RD Mailform-->
      <form class="text-left" action="{{url('sendemail/send')}}" method="post" >
        @csrf
        <div class="row row-15">
          <div class="col-sm-6">
            <div class="form-wrap">
              <label class="form-label" for="contact-name">First name</label>
              <input class="form-input" id="contact-name" type="text" name="name" required="">
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-wrap">
              <label class="form-label" for="contact-sec-name">Last name</label>
              <input class="form-input" id="contact-sec-name" type="text" name="sec_name" required="">
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-wrap">
              <label class="form-label" for="contact-phone">Phone</label>
              <input class="form-input" id="contact-phone" type="text" name="phone" required="">
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-wrap">
              <label class="form-label" for="contact-email">E-Mail</label>
              <input class="form-input" id="contact-email" type="email" name="email" required="">
            </div>
          </div>
          <div class="col-12">
            <div class="form-wrap">
              <label class="form-label" for="contact-message">Message</label>
              <textarea class="form-input" id="contact-message" name="message" required=""></textarea>
            </div>
          </div>
        </div>
        <div class="form-button group-sm text-left">
          <button class="button button-primary" type="submit">Send message</button>
        </div>
      </form>
    </div>
    <div class="col-lg-4">
      <ul class="contact-list">
        <li> 
          <p class="contact-list-title">Address</p>
          <div class="contact-list-content"><span class="icon mdi mdi-map-marker icon-primary"></span><a href="1234567890">Washington, USA 6036 Richmond hwy., Alexandria, VA, 2230 </a></div>
        </li>
        <li>
          <p class="contact-list-title">Phone</p>
          <div class="contact-list-content"><span class="icon mdi mdi-phone icon-primary"></span><a href="tel:1234567890">+1 (409) 987–5874</a><span>, </span><a href="tel:#">+1(409) 987–5884 </a></div>
        </li>
        <li>
          <p class="contact-list-title">E-mail</p>
          <div class="contact-list-content"><span class="icon mdi mdi-email-outline icon-primary"></span><a href="mailto:testmail@gmail.com"><span>testmail@gmail.com</span></a></div>
        </li>
        <li>
          <p class="contact-list-title">Opening Hours</p>
          <div class="contact-list-content"><span class="icon mdi mdi-clock icon-primary"></span>
            <ul class="list-xs">
              <li>Mon-Fri: 9 am – 6 pm</li>
              <li>Saturday: 9 am – 4 pm</li>
              <li>Sunday: Closed</li>
            </ul>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>
</section>
<!-- Page Footer-->
<!-- Google map-->
<section class="section">
  <div class="google-map-container">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d16300690.430084962!2d100.57057402696853!3d4.089292544186079!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3034d3975f6730af%3A0x745969328211cd8!2sMalaysia!5e0!3m2!1sen!2snp!4v1564483189428!5m2!1sen!2snp" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
  </div>
</section>
@endsection