 @extends('layouts.new.app', ['title' => 'Gallery'],['discription'=> 'Flower Website Our Flower brings unique and custom Flower designs to all customers of LA area.'])

@section('content')
<section class="section-page-title" style="background-image: url(images/page-title-2-1920x305.jpg); background-size: cover;">
        <div class="container">
          <h1 class="page-title">Grid Gallery</h1>
        </div>
      </section>
      <section class="breadcrumbs-custom">
        <div class="container">
          <ul class="breadcrumbs-custom-path">
            <li><a href="/">Home</a></li>
            <li class="active">Gallery</li>
          </ul>
        </div>
      </section>
      <section class="section section-lg bg-default text-center">
        <div class="container">
          <h2>All Gallery</h2>
          <div class="divider-lg"></div>
          <p class="block-lg">Check out the full portfolio of our works including haircuts, custom makeup, exclusive manicure and pedicure, and more. All these works were performed by our skilled estheticians, manicurists, and cosmetologists.</p>
          <div class="row row-30">
            <!-- Isotope Filters-->
            <div class="col-lg-12">
              <div class="isotope-filters isotope-filters-horizontal">
                <button class="isotope-filters-toggle button button-sm button-primary" data-custom-toggle="#isotope-filters" data-custom-toggle-disable-on-blur="true">Filter<span class="caret"></span></button>
                <ul class="isotope-filters-list" id="isotope-filters">
                  <li><a class="active" data-isotope-filter="*" data-isotope-group="gallery" href="#">All</a></li>
                  @foreach($gallerycategories as $gallerycat)
                  <li><a data-isotope-filter="{{$gallerycat->id}}" data-isotope-group="gallery" href="#">{{$gallerycat->title}} </a></li>
                  @endforeach
                </ul>
              </div>
            </div>
            <!-- Isotope Content-->
                        <div class="col-lg-12">
              <div class="isotope row" data-isotope-layout="masonry" data-isotope-group="gallery" data-lightgallery="group" data-column-class=".col-sm-6.col-lg-4">
                      @foreach($gallerycategories as $gallcat)
            @foreach ($gallery as $galle)
            @if($gallcat->id === $galle->cat_id)
                <div class="col-sm-6 col-lg-4 isotope-item" data-filter="{{$galle->cat_id}}"><a class="gallery-item" data-lightgallery="item" href="/uploads/gallery/{{$galle->file}}"><img src="/uploads/gallery/{{$galle->file}}" alt="" width="570" height="570"/></a>
                </div>
                @endif
                @endforeach
            @endforeach
              </div>
            </div>
            <div class="col-12">
                  <ul class="pagination">
                    <li style="margin: 0 auto;" class="page-item"></li>
                  </ul>
                </div>
          </div>
        </div>
      </section>
      <!-- Page Footer-->
      @endsection