 @extends('layouts.new.app', ['title' => 'Blog Category'],['discription'=> 'Flower Website Our Flower brings unique and custom Flower designs to all customers of LA area.'])

@section('content')
<style type="text/css">
                  .gallery-item{
                    height: auto;
                  }
                </style>
<section class="section-page-title" style="background-image: url(/images/page-title-5-1920x305.jpg); background-size: cover;">
        <div class="container">
          <h1 class="page-title">Blogs</h1>
        </div>
      </section>
      <section class="breadcrumbs-custom">
        <div class="container">
          <ul class="breadcrumbs-custom-path">
            <li><a href="/">Home</a></li>
            <li><a href="/blogs">Blog</a></li>
            <li style="text-transform: capitalize;" class="active">{{ $blogCatName }}</li>
          </ul>
        </div>
      </section>
      <section class="section section-lg bg-default">
        <div class="container">
          <div class="row row-50">
            <div class="col-lg-8">
              <div class="row row-50">
                <div class="col-lg-11">
                  @if(count($blogs))
                  @foreach($blogs as $blog)
                  @if($blog->cat_id == $id)
                  <div class="post-corporate">
                    <div class="post-corporate-img"><a href="/blog-detail/{{$blog->id}}"><img src="/uploads/{{$blog-> f_image}}" alt="{{$blog-> title}}" width="735" height="400"/></a>
                     <!--  <ul class="tag-list"> 
                        <li><a href="#">Beauty</a></li>
                        <li><a href="#">Salon</a></li>
                      </ul> -->
                    </div>
                    <div class="post-corporate-caption">
                      <h4 class="post-corporate-title"><a href="/blog-detail/{{$blog->id}}">{{$blog->title}}</a></h4>
                      <p><?php echo ($blog -> description)?></p>
                      <ul class="post-corporate-meta-list">
                        <li class="time">{{ $blog->created_at->format('d M , Y') }}</li>
                        <li>by Admin </li>
                      </ul>
                    </div>
                  </div>
                  @endif
                  @endforeach
                      @else
                      <h5>There are no posts in Blog.</h5>
                      @endif
                </div>
                <div class="col-12">
                  <ul class="pagination">
                    <li class="page-item">{{ $blogs->links() }}</li>
                    <!-- <li class="page-item page-item-control"><a class="page-link" href="#" aria-label="Previous">prev</a></li>
                    <li class="page-item active"><span class="page-link">1</span></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">4</a></li>
                    <li class="page-item page-item-control"><a class="page-link" href="#" aria-label="Next">next</a></li> -->
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="blog-aside-list">
                <div class="blog-aside-list-item">
                  <h4 class="blog-aside-list-item-title">Categories</h4>
                  <ul class="list-marked">
                    <li><a href="/blogs">All</a></li>
                    @foreach($blogcategories as $blogcategory)
                    <li><a href="/blogs/category/{{$blogcategory->id}}">{{$blogcategory->title}}</a></li>
                    @endforeach
                  </ul>
                </div>
<!--                 <div class="blog-aside-list-item">
                  <h4 class="blog-aside-list-item-title">Search</h4>
                  <form class="rd-search" action="search-results" method="GET" data-search-live="rd-search-results-live">
                    <div class="form-wrap">
                      <label class="form-label" for="rd-search-form-input">Search...</label>
                      <input class="form-input" id="rd-search-form-input" type="text" name="s" autocomplete="off">
                      <button class="icon mdi mdi-magnify" type="submit"></button>
                    </div>
                  </form>
                </div> -->
                <!-- <div class="blog-aside-list-item">
                  <h4 class="blog-aside-list-item-title">Archive</h4>
                  <ul class="list-marked list-two-column">
                    <li><a href="#">Jun 2018</a></li>
                    <li><a href="#">Jul 2018</a></li>
                    <li><a href="#">Aug 2018</a></li>
                    <li><a href="#">Sep 2018</a></li>
                    <li><a href="#">Oct 2018</a></li>
                    <li><a href="#">Nov 2018</a></li>
                    <li><a href="#">Dec 2018</a></li>
                    <li><a href="#">Jan 2018</a></li>
                    <li><a href="#">Feb 2018</a></li>
                    <li><a href="#">Mar 2018</a></li>
                  </ul>
                </div> -->
                
               <div class="blog-aside-list-item">
                  <h4 class="blog-aside-list-item-title">Gallery</h4>
                  <div class="blog-aside-gallery" data-lightgallery="group">
                    <?php $count=1; ?>
                    @foreach($gallery as $galley)
                    <?php 
                    $string = $galley->image;
                    $string = substr(strrchr($string, '.'), 1);
                ?>
                @if($string == "jpg" || $string == "png" || $string == "jpeg")
                    <div class="blog-aside-gallery-item"><a class="gallery-item" href="/uploads/gallery/{{$galley->image}}" data-lightgallery="item"><img src="/uploads/gallery/{{$galley->image}}" alt="" width="180" height="140"/></a></div>
                    @endif
                    <?php $count ++; ?>
                    <?php if($count > 6)
                    break
                    ?>
                    @endforeach
                  </div>
                </div>
                <div class="blog-aside-list-item">
                  <h4 class="blog-aside-list-item-title">Recent Blog Posts</h4>
                  <?php $count=1; ?>
                  @if(count($blogs))
                  @foreach($blogs as $blog)
                  <div class="blog-aside-post">
                    <h5><a href="/blog-detail/{{$blog->id}}">{{$blog->title}}</a></h5>
                    <p>{{ $blog->created_at->format('D M , Y | H:i') }}</p>
                  </div>
                  <?php $count ++; ?>
                  <?php if($count > 3)
                  break
                  ?>
                  @endforeach
                  @else
                  <h5>There are no posts in Blog.</h5>
                  @endif
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Page Footer-->
      @endsection