@extends('layouts.main')

@section('content')
<div class="breadcrumbs">
	<div class="container">
		<div class="row">
			<div class="breadcrumbs__title">
				<h5 class="page-title">Contact Us</h5>
			</div>
			<div class="breadcrumbs__items">
				<div class="breadcrumbs__content">
					<div class="breadcrumbs__wrap">
						<div class="breadcrumbs__item">
							<a href="/" class="breadcrumbs__item-link is-home" rel="home" title="Home">Home</a></div>
							<div class="breadcrumbs__item">
								<div class="breadcrumbs__item-sep">/</div>
							</div>
							<div class="breadcrumbs__item"><span class="breadcrumbs__item-target">Contact Us</span></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="map">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d16300690.430084962!2d100.57057402696853!3d4.089292544186079!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3034d3975f6730af%3A0x745969328211cd8!2sMalaysia!5e0!3m2!1sen!2snp!4v1563180927446!5m2!1sen!2snp" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>
	<section data-id="sdweqlq" class="element element-sdweqlq section-boxed section-height-default section-height-default section top-section jet-parallax-section" data-element_type="section">
		<div class="container column-gap-default">
			<div class="row">
				<div class="widget-container">
					<div class="icon-box-wrapper row">
						<div class="icon-box-icon col-sm-5">
							<span class="icon animation-">
								<i class="fa fa-phone" aria-hidden="true"></i>
							</span>
						</div>
						<div class="icon-box-content col-sm-7">
							<h6 class="icon-box-title">
								<span>Phone:</span>
							</h6>
							<p class="icon-box-description"><a href="tel:+1234567890">+1234567890; +1234567890</a></p>
						</div>
					</div>
				</div>
				<div class="icon-box-wrapper row">
					<div class="icon-box-icon col-sm-5"> 
						<span class="icon animation-">
							<i class="fa fa-map-marker" aria-hidden="true"></i>
						</span>
					</div>
					<div class="icon-box-content col-sm-7">
						<h6 class="icon-box-title">
							<span>Address:</span>
						</h6>
						<p class="icon-box-description">4578 ABC Road, XYZ</p>
					</div>
				</div>
				<div class="icon-box-wrapper row">
					<div class="icon-box-icon col-sm-5">
						<a class="icon animation-" href="mailto:info@demolink.org">
							<i class="fa fa-envelope"></i>
						</a>
					</div>
					<div class="icon-box-content col-sm-7">
						<h6 class="icon-box-title">
							<span>E-mail:</span>
						</h6>
						<p class="icon-box-description"><a href="mailto:info@demolink.com">info@demolink.com</a></p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<form method="post" action="/sendemail/send" role="form">

    <div class="messages"></div>

    <div class=" container controls">

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="form_name">Name *</label>
                    <input id="form_name" type="text" name="name" class="form-control" placeholder="Please enter your name *" required="required" data-error="Name is required.">
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <label for="form_email">Email *</label>
                    <input id="form_email" type="email" name="email" class="form-control" placeholder="Please enter your email *" required="required" data-error="Valid email is required.">
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <label for="form_address">Address *</label>
                    <input id="form_address" type="text" name="address" class="form-control" placeholder="Please enter your address *" required="required" data-error="Address is required.">
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <label for="form_need">Please specify your need *</label>
                    <select id="form_need" name="need" class="form-control" required="required" data-error="Please specify your need.">
                        <option value=""></option>
                        <option value="Request quotation">Request quotation</option>
                        <option value="Request order status">Request order status</option>
                        <option value="Request copy of an invoice">Request copy of an invoice</option>
                        <option value="Other">Other</option>
                    </select>
                    <div class="help-block with-errors"></div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="form_message">Message *</label>
                    <textarea id="form_message" name="message" class="form-control" placeholder="Message for me *" rows="8" required="required" data-error="Please, leave us a message."></textarea>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
            	<p class="text-muted">
                    <strong>*</strong> These fields are required.<br> We will contact you within one business day.</p>
            </div>
            <div class="col-md-6">
            	<input style="float: right;" type="submit" class="btn contact_button" value="Send message">
            </div>
        </div>
    </div>

</form>
		@endsection