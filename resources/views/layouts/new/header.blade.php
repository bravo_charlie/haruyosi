
    <div class="page">
      <!-- Page Header-->
      <header class="section page-header">
        <!-- RD Navbar-->
        <div class="rd-navbar-wrap">
          <nav class="rd-navbar rd-navbar-classic rd-navbar-classic-minimal" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-lg-device-layout="rd-navbar-static" data-xl-layout="rd-navbar-static" data-xl-device-layout="rd-navbar-static" data-lg-stick-up-offset="46px" data-xl-stick-up-offset="46px" data-xxl-stick-up-offset="46px" data-lg-stick-up="true" data-xl-stick-up="true" data-xxl-stick-up="true">
            <div class="rd-navbar-aside-outer rd-navbar-collapse toggle-original-elements">
              <div class="rd-navbar-aside">
                <div class="header-info">
                  <ul class="list-inline list-inline-md">
                    <li>
                      <div class="unit unit-spacing-xs align-items-center">
                        <div class="unit-left font-weight-bold">Free Call:</div>
                        <div class="unit-body"><a href="tel:#">(073) 123-12-12</a></div>
                      </div>
                    </li>
                    <li>
                      <div class="unit unit-spacing-xs align-items-center">
                        <div class="unit-left font-weight-bold">Opening Hours: </div>
                        <div class="unit-body"> Mn-Fr: 10 am-8 pm</div>
                      </div>
                    </li>
                  </ul>
                </div>
                <div class="social-block">
                  <ul class="list-inline">
                    <li><a class="icon fa-facebook" href="#"></a></li>
                    <li><a class="icon fa-twitter" href="#"></a></li>
                    <li><a class="icon fa-google-plus" href="#"></a></li>
                    <li><a class="icon fa-vimeo" href="#"></a></li>
                    <li><a class="icon fa-youtube" href="#"></a></li>
                    <li><a class="icon fa-pinterest-p" href="#"></a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="rd-navbar-main-outer">
              <div class="rd-navbar-main">
                <!-- RD Navbar Panel-->
                <div class="rd-navbar-panel">
                  <!-- RD Navbar Toggle-->
                  <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button>
                  <!-- RD Navbar Brand-->
                  <!-- <div class="rd-navbar-brand"><a class="brand" href="/new"><img src="/images/haruyosi-logo.png" alt="" width="158" height="58"/></a></div> -->
                  <span><a class="rd-nav-link" href="/"><h2 class="text-primary">HARUYOSI</h2></a></span>
                </div>
                <div class="rd-navbar-main-element">
                  <div class="rd-navbar-nav-wrap">
                    <!-- RD Navbar Nav-->
                    <ul class="rd-navbar-nav">
                      <li class="rd-nav-item {{ Request::is('/') ? 'active' : '' }}"><a class="rd-nav-link" href="{{ url('/' )}}">Home</a>
                      <li class="rd-nav-item {{ Request::segment(1)=='about-us' ? 'active' : '' }}"><a class="rd-nav-link" href="/about-us">About</a>

                      <li class="rd-nav-item {{ Request::segment(1)=='services' ? 'active' : '' }}"><a class="rd-nav-link" href="/services">Services</a>
                      </li>
                      <li class="rd-nav-item {{ Request::segment(1)=='gallery' ? 'active' : '' }}"><a class="rd-nav-link" href="/gallery">Gallery</a>
                        <!-- RD Navbar Dropdown-->
                     <!--    <ul class="rd-menu rd-navbar-dropdown">
                          <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="/grid-gallery">Grid Gallery</a></li>
                          <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="/cobbles-gallery">Cobbles Gallery</a></li>
                          <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="/masonry-gallery">Masonry Gallery</a></li>
                          <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="/gallery-without-padding">Gallery without padding</a></li>
                        </ul> -->
                      </li>

                      <li class="rd-nav-item {{ Request::segment(1)=='blogs' ? 'active' : '' }}"><a class="rd-nav-link" href="/blogs">Blog</a></li>
                      <li class="rd-nav-item {{ Request::segment(1)=='contacts' ? 'active' : '' }}"><a class="rd-nav-link" href="/contacts">Contacts</a></li>
                        </ul>
                      </li>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </nav>
        </div>
      </header>