@extends('layouts.main')

@section('content')
<!-- <div class="container-fluid header_part">
  <div style="margin-right: 0;" class="row">
    <div class="col-sm-5 menu_bar align-middle">
      <nav class="navbar navbar-expand-lg navbar-light">

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
               Shop Gift Boxes
             </a>
             <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="#">All</a>
              <a class="dropdown-item" href="#">Baby</a>
              <a class="dropdown-item" href="#">Christmas</a>
              <a class="dropdown-item" href="#">Foodie</a>
              <a class="dropdown-item" href="#">For Her</a>
              <a class="dropdown-item" href="#">For Him</a>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Corporate
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="#">Corporate</a>        
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">About Us</a>
          </li>
        </ul>
      </div>
    </nav>
  </div>
  <div class="col-sm-2 menu_logo">
    <a href="/"><img src="{{asset('/images/main_logo.png')}}"></a>
  </div>
  <div class="col-sm-5 align-middle">
    <div class="text-center logo_right" id="navbarResponsive">
     <ul class="">
      <li class=""><a href="#"><i class="fa fa-envelope"></i></a></li>
      <li class=""><a href="#"><i class="fa fa-facebook"></i></a></li>
      <li class=""><a href="#"><i class="fa fa-pinterest"></i></a></li>
      <li class=""><a href="#"><i class="fa fa-linkedin"></i></a></li>
      <li class=""><a href="#"><i class="fa fa-instagram"></i></a></li>
    </ul>
  </div>
</div>
</div>
</div> -->
<section>
      <div style="min-height: 50px;">
        <!-- Jssor Slider Begin -->
        
        <style>
            /* jssor slider loading skin spin css */
            .jssorl-009-spin img {
                animation-name: jssorl-009-spin;
                animation-duration: 1.6s;
                animation-iteration-count: infinite;
                animation-timing-function: linear;
            }

            @keyframes jssorl-009-spin {
                from {
                    transform: rotate(0deg);
                }

                to {
                    transform: rotate(360deg);
                }
            }
        </style>
        <div id="slider1_container" style="visibility: hidden; position: relative; margin: 0 auto;
        top: 0px; left: 0px; width: 1300px; height: 500px; overflow: hidden;">
            <!-- Loading Screen -->
            <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
                <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="{{asset('/svg/loading/static-svg/spin.svg')}}" />
            </div>

            <!-- Slides Container -->
            <div data-u="slides" style="position: absolute; left: 0px; top: 0px; width: 1300px; height: 500px; overflow: hidden;">
                <div>
                    <img data-u="image" src="{{asset('/images/slider/slider1.jpg')}}" />
                    <span class="slider_text"><h2>Meaningful Gifts for Magical Moments</h2><a href="#" class="btn-slider">SHOP NOW -></a></span>
                </div>
                <div>
                    <img data-u="image" src="{{asset('/images/slider/slider2.jpg')}}" />
                </div>
                <div>
                    <img data-u="image" src="{{asset('/images/slider/slider3.jpg')}}" />
                </div>
            </div>
            
            <div data-u="navigator" class="jssorb031" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
                <div data-u="prototype" class="i" style="width:16px;height:16px;">
                    <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                        <circle class="b" cx="8000" cy="8000" r="5800"></circle>
                    </svg>
                </div>
            </div>
            <!--#endregion Bullet Navigator Skin End -->

            <div data-u="arrowleft" class="jssora051" style="width:55px;height:55px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
                <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
                </svg>
            </div>
            <div data-u="arrowright" class="jssora051" style="width:55px;height:55px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
                <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
                </svg>
            </div>
            <!--#endregion Arrow Navigator Skin End -->
        </div>
        <!-- Jssor Slider End -->
    </div>
</section>
<section class="about_part">
  <div class="about_section_img">
    <img src="{{asset('/images/about-image.png')}}">
  </div>
  <div class="about_description">
    <h2 style="color:#b8958a;text-align: center;text-transform: uppercase;font-size: 28px; white-space: pre-wrap;">The gift boxes that keep on giving</h2>
    <p style="text-align: center; white-space: pre-wrap;">Carefully curated and thoughtfully put together, our beautiful gift boxes are the perfect combination of forever-after keepsakes and in-the-moment treats.</p>
    <p style="text-align: center; white-space: pre-wrap;">Featuring the finest New Zealand-made products and naturally packaged in a bespoke, wooden box, each and every gift leaves a lasting impression. Because a little luxury goes a long way.</p>
  </div>
  <div class="about_btn">
  <a href="">Browse our gift boxes</a>
    
  </div>
</section>
<section class="shop-now">
  <div class="shop_heading">
    <h2>SHOP NOW</h2>
    <p>Packed full of ethically and sustainably made products chosen for their quality and craft, we have gift boxes for birthdays, weddings, newborns or just because.</p>
  </div>
  <div class="container container_edit">
    <div class="row">
      <div class="col-sm-4 product">
        <img src="/images/shop-item1.jpg" alt="a">
        <div class="product_description">
        <div class="product_title"><p>Pamper Her</p></div>
        <div class="product_button"><a href="#">Shop Now</a></div>
      </div>
      </div>
      <div class="col-sm-4 product">
        <img src="/images/shop-item2.jpg" alt="b">
        <div class="product_description">
        <div class="product_title"><p>Pamper Him</p></div>
        <div class="product_button"><a href="#">Shop Now</a></div>
      </div>
      </div>
      <div class="col-sm-4 product">
        <img src="/images/shop-item3.jpg" alt="c">
        <div class="product_description">
        <div class="product_title"><p>Fine Foodie</p></div>
        <div class="product_button"><a href="#">Shop Now</a></div>
      </div>
      </div>
    </div>
  </div>
</section>
<section class="quote_block">
  <div class="quote_part">
    <h3>“I adore giving gifts that I’ve put a lot of thought and care into. It’s the most unique way to honour someone, and it makes the moment truly memorable, both for the giver and the receiver.”</h3>
    <p>— GILL, FOUNDER OF D’LUXE GIFTING</p>
  </div>
</section>
<section>
  <div class="testimonial">
    <div class="container container_edit">
      <div class="row">
        <div class="col-sm-5">
          <img style="max-width: 100%;" src="/images/testimonial_image.jpg" alt="testimonial">
        </div>
        <div class="col-sm-1"></div>
        <div class="col-sm-6">
          <div class="make_space"></div>
          <div class="sqs-block-content"><h2 style="white-space: pre-wrap;color: #b8958a;">Kind Words</h2></div>
               <div id="myCarousel" class="carousel slide" data-ride="carousel">   
        <!-- Wrapper for carousel items -->
        <div class="carousel-inner">
          <div class="item carousel-item active">
                <div class="testimonial_part">
                  <p>— LP - WAKEFIELD, DECEMBER 2018</p>
                  <h3>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante.'</h3>
                </div>
          </div>
          <div class="item carousel-item">
                <div class="testimonial_part">
                  <p>— TW - NELSON, SEPTEMBER 2018</p>
                  <h3>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante.'</h3>
            </div>      
          </div>
          <div class="item carousel-item">
                <div class="testimonial_part">
                  <p>— JB - TIMARU, AUGUST 2018</p>
                  <h3>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante.'</h3>
            </div>      
          </div>
        </div>
        <!-- Carousel controls -->
        <a class="carousel-control left carousel-control-prev" href="#myCarousel" data-slide="prev">
          <i class="fa fa-chevron-left"></i>
        </a>
        <a class="carousel-control right carousel-control-next" href="#myCarousel" data-slide="next">
          <i class="fa fa-chevron-right"></i>
        </a>
      </div>
      <div class="testimonial_btn">
        <div style="text-align: -webkit-auto;" class="about_btn">
  <a href="">READ MORE KIND WORDS</a>
    
  </div>
      </div>
    </div>
  </div>
</div>
</div>
</section>
<section class="gift_section">
  <div class="about_description">
    <h2 style="color:#b8958a;text-align: center;text-transform: uppercase;font-size: 28px; white-space: pre-wrap;">The gift boxes that keep on giving</h2>
    <p style="text-align: center; white-space: pre-wrap;">Carefully curated and thoughtfully put together, our beautiful gift boxes are the perfect combination of forever-after keepsakes and in-the-moment treats.</p>
  </div>
  <div class="about_btn">
  <a href="">SHOP GIFT BOXES NOW</a>
    
  </div>
</section>
@endsection