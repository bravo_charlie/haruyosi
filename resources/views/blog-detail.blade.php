@extends('layouts.main')
<link rel="stylesheet" type="text/css" href="{{asset('css/blog.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/blog_responsive.css')}}">

@section('content')
<!-- Home -->

<div class="home home_blog_detail d-flex flex-column align-items-start justify-content-end">
	<div class="parallax_background parallax-window" data-parallax="scroll" data-speed="0.8"></div>
	<div class="home_container">
		<div class="container">
			<div class="row">
				<div class="col">
					<!-- <div class="home_content">
						<div class="home_title">Blog Detail</div>
						<div class="home_text">Welcome to our company.</div>
					</div> -->
				</div>
			</div>
		</div>
	</div>

</div>

	<!-- category -->
	<section class="category_color">	
		<div class="row">
						<div class="sub_category">
							<div class="inner">
								<ul class="primary-blog">
									@foreach($blogcategories as $blogcategory)
									<li class="blog-item"><a href="/blogs/category/{{$blogcategory->id}}">{{$blogcategory->title}}</a></li>
									@endforeach
								</ul>
							</div>
						</div>
						<div class="cat_button" style="position: absolute;">
<button class="previous round" id="left">&#8249;</button>
<button class="next round" id="right">&#8250;</button>
</div>
					</div>

	</section>
<!-- Blog -->

<div class="blog">
	<div class="container">
		<div class="row">
			<div class="col">

				<!-- Blog Post -->
				<div class="blog_post">
					<div class="row">
					<div class="col-sm-6 blog_post_image-my"><img src="/uploads/{{$blogs-> f_image}}" alt="{{$blogs->name}}"></div>
					<br>
					<div class="col-sm-6 blog_post_image-my"><img src="/uploads/{{$blogs-> i_image}}" alt="{{$blogs->name}}"></div>
				</div>
</div>					<div class="blog_post_date d-flex flex-column align-items-center justify-content-center">
						<div class="date_day">{{ $blogs->created_at->format('d') }}</div>
						<div class="date_month">{{ $blogs->created_at->format('M') }}</div>
						<div class="date_year">{{ $blogs->created_at->format('Y') }}</div>
					</div>
					<div class="blog_post_info">
						<ul class="d-flex flex-row align-items-center justify-content-center">
							<li>By : <a >Admin</a></li>
							<li>In : <a >
								@foreach($blogcategories as $blogcategory)
								@if($blogs->cat_id === $blogcategory->id)
								{{$blogcategory->title}}
								@endif
								@endforeach
							</a></li>
						</ul>
					</div>
					<div class="blog_post_title"><a style="color: #000000;" >{{$blogs->title}}</a></div>	
					<div class=" text-center-des">
						<p><?php echo ($blogs->description)?></p>
					</div>

				</div>

			</div>
		</div>
	</div>




@endsection