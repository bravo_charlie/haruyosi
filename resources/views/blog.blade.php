@extends('layouts.main')
<link rel="stylesheet" type="text/css" href="{{asset('css/blog.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/blog_responsive.css')}}">

@section('content')

<!-- Home -->
<div class="home home_blog_detail d-flex flex-column align-items-start justify-content-end">
	<div class="parallax_background parallax-window" data-parallax="scroll" data-speed="0.8"></div>
	<div class="home_container">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="home_content">
						<div class="home_title banner">Blogs</div>
						<div class="home_text">Welcome to our company.</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	<!-- category -->
	<section class="category_color">	
		<div style="top: 8px;" class="row">
						<div class="sub_category">
							<div class="inner">
								<ul class="primary-blog">
									@foreach($blogcategories as $blogcategory)
									<li class="blog-item"><a href="/blogs/category/{{$blogcategory->id}}">{{$blogcategory->title}}</a></li>
									@endforeach
								</ul>
							</div>
						</div>
						<div class="cat_button" style="position: absolute;">
<button class="previous round" id="left">&#8249;</button>
<button class="next round" id="right">&#8250;</button>
</div>
					</div>

	</section>
	  

<!-- Blog -->

<section class="top_blogs blog">
	<div style="max-width: 65.5%;border:none;padding: 0;" class="container company_overview blog_post_res">
		<div>
		<h1>LATEST FROM THE BLOG</h1>
		<p>news, events, and tips from our team</p>
	</div>
	<div class="row">
		@if(count($blogs))
			@foreach($blogs as $blog)
		<div class="col-sm-6">
		<div class="main_image_blog">
			<div class="blog_post_date d-flex flex-column align-items-center justify-content-center">
						<div class="date_day">{{ $blog->created_at->format('d') }}</div>
						<div class="date_month">{{ $blog->created_at->format('M') }}</div>
						<div class="date_year">{{ $blog->created_at->format('Y') }}</div>
					</div>
			<a href="/blog-detail/{{$blog->id}}"><img src="uploads/{{$blog-> f_image}}" alt="{{$blog-> title}}"></a>
		</div>
		<div class="blog_post_info">
						<ul class="d-flex flex-row align-items-center justify-content-center">
							<li>By : <a >Admin</a></li>
							<li>In : <a >
								@foreach($blogcategories as $blogcategory)
								@if($blog->cat_id === $blogcategory->id)
								{{$blogcategory->title}}
								@endif
								@endforeach
							</a></li>
						</ul>
					</div>
		<div class="main_text_blog">
			<h6><a href="/blog-detail/{{$blog->id}}">{{$blog->title}}</a></h6>
		<p><?php echo ($blog -> description)?></p>
		</div>
		<div class="blog_post_button"><div class="button button_1 ml-auto mr-auto"><a href="/blog-detail/{{$blog->id}}">read more</a></div></div>
	</div>
	@endforeach
			@else
			<h5>There are no posts in Blog.</h5>
			@endif
</div>
<div class="row page_nav_row">
			<div class="col">
				<div class="page_nav">
					<ul class="d-flex flex-row align-items-center justify-content-center">
						<li>{{ $blogs->links() }}</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Newsletter -->

<div class="newsletter">
	<div class="parallax_background parallax-window" data-parallax="scroll" data-speed="0.8"></div>
	<div class="container newsletter_background">
		<div class="row">
			<div class="col text-center">
				<div class="newsletter_title">Subscribe to our newsletter</div>
			</div>
		</div>
		<div class="row newsletter_row">
			<div class="col-lg-8 offset-lg-2">
				<div class="newsletter_form_container">
					<form action="/subscribe/send" method="post" id="newsleter_form" class="newsletter_form">
						@csrf()
						<input type="email" name="email" class="newsletter_input" placeholder="Your E-mail" required="required">
						<button class="newsletter_button">subscribe</button>
						<br><br>
						@if (count($errors) > 0)
						<div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert">×</button>
							<ul>
								@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
						@endif
						@if ($message = Session::get('success'))
						<div class="alert alert-success alert-block">
							<button type="button" class="close" data-dismiss="alert">×</button>
							<strong>{{ $message }}</strong>
						</div>
						@endif
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- 	<script>
		var outer = $('.sub_category');

		$('#right').click(function() {
		   outer.animate({scrollLeft: '+=300px'}, 1140);
		});

		$('#left').click(function() {
		   outer.animate({scrollLeft: '-=300px'}, 1140);
		});
	</script> -->
@endsection