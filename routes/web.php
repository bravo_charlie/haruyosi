<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/w', function () {
    return view('welcome');
});
Route::get('/newblog', function () {
    return view('newblog');
});
Route::get('/blog-post', function () {
    return view('blog-post');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::fallback(function() {
    return view('nopage');
});

Route::get('/','MainPageController@index');
Route::get('/gallery','MainPageController@galleryPage');
Route::get('/about-us','MainPageController@aboutPage');
Route::get('/services','MainPageController@servicePage');
Route::get('/blogs','MainPageController@blogIndex');
Route::get('/blogs/category/{id}','MainPageController@blogCategoryPage');
Route::get('/blog-detail/{id}','MainPageController@blogDetail');
Route::get('/contacts','MainPageController@contact');
Route::get('/training','MainPageController@trainingPage');

Route::post('/sendemail/send', 'SendMailController@send');
Route::post('/subscribe/send', 'SendMailController@subscribe');

Route::get('/home/users', 'UserPasswordController@index');
Route::get('/home/users/create', 'UserPasswordController@create');
Route::post('/home/users/create', 'UserPasswordController@store')->name('make.user');
Route::get('/home/users/{id}', 'UserPasswordController@show');
Route::get('/home/users/edit/{id}', 'UserPasswordController@edit');
Route::post('/home/users/edit/{id}', 'UserPasswordController@update')->name('update.user');
Route::get('/home/users/destroy/{id}', 'UserPasswordController@destroy')->name('delete.user');

Route::get('/home/services', 'ServiceController@index');
Route::get('/home/services/create', 'ServiceController@create');
Route::post('/home/services/create', 'ServiceController@store');
Route::get('/home/services/edit/{id}', 'ServiceController@edit')->name('service.edit');
Route::post('/home/services/edit/{id}', 'ServiceController@update')->name('service.update');
Route::delete('/home/services/destroy/{id}', 'ServiceController@destroy')->name('service.delete');

Route::get('/home/gallery-category', 'GallerycategoryController@index');
Route::get('/home/gallery-category/create', 'GallerycategoryController@create');
Route::post('/home/gallery-category/create', 'GallerycategoryController@store')->name('make.gallery');
Route::get('/home/gallery-category/{id}', 'GallerycategoryController@show');
Route::get('/home/gallery-category/edit/{id}', 'GallerycategoryController@edit');
Route::post('/home/gallery-category/edit/{id}', 'GallerycategoryController@update')->name('update.gallery');
Route::get('/home/gallery-category/destroy/{id}', 'GallerycategoryController@destroy')->name('delete.gallery');


Route::get('/home/gallery', 'GalleryController@index');
Route::get('/home/gallery/create', 'GalleryController@create');
Route::post('/home/gallery/create', 'GalleryController@store');
// Route::get('/home/gallery/createVideo', 'GalleryController@createVideo');
// Route::post('/home/gallery/createVideo', 'GalleryController@storeVideo');
Route::get('/home/gallery/edit/{id}', 'GalleryController@edit')->name('gallery.edit');
Route::post('/home/gallery/edit/{id}', 'GalleryController@update')->name('gallery.update');
Route::delete('/home/gallery/destroy/{id}', 'GalleryController@destroy')->name('gallery.delete');


Route::get('/home/blog-category', 'BlogcategoryController@index');
Route::get('/home/blog-category/create', 'BlogcategoryController@create');
Route::post('/home/blog-category/create', 'BlogcategoryController@store')->name('make.blog');
Route::get('/home/blog-category/{id}', 'BlogcategoryController@show');
Route::get('/home/blog-category/edit/{id}', 'BlogcategoryController@edit');
Route::post('/home/blog-category/edit/{id}', 'BlogcategoryController@update')->name('update.blog');
Route::get('/home/blog-category/destroy/{id}', 'BlogcategoryController@destroy')->name('delete.blog');


Route::get('/home/blogs', 'BlogController@index');
Route::get('/home/blog/create', 'BlogController@create');
Route::post('/home/blog/create', 'BlogController@store')->name('make-post.blog');
Route::get('/home/blog/{id}', 'BlogController@show');
Route::get('/home/blog/edit/{id}', 'BlogController@edit');
Route::post('/home/blog/edit/{id}', 'BlogController@update')->name('update-post.blog');
Route::get('/home/blog/destroy/{id}', 'BlogController@destroy')->name('delete-post.blog');


Route::get('/home/sliders', 'SliderController@index');
Route::get('/home/slider/create', 'SliderController@create');
Route::post('/home/slider/create', 'SliderController@store');
Route::get('/home/slider/edit/{id}', 'SliderController@edit')->name('slider.edit');
Route::post('/home/slider/edit/{id}', 'SliderController@update')->name('slider.update');
Route::delete('/home/slider/destroy/{id}', 'SliderController@destroy')->name('slider.delete');


Route::get('/home/testimonials', 'TestimonialController@index');
Route::get('/home/testimonials/create', 'TestimonialController@create');
Route::post('/home/testimonials/create', 'TestimonialController@store');
Route::get('/home/testimonials/edit/{id}', 'TestimonialController@edit')->name('testimonial.edit');
Route::post('/home/testimonials/edit/{id}', 'TestimonialController@update')->name('testimonial.update');
Route::delete('/home/testimonials/destroy/{id}', 'TestimonialController@destroy')->name('testimonial.delete');

Route::get('/home/staffs', 'StaffController@index');
Route::get('/home/staffs/create', 'StaffController@create');
Route::post('/home/staffs/create', 'StaffController@store')->name('make.staff');
Route::get('/home/staffs/{id}', 'StaffController@show');
Route::get('/home/staffs/edit/{id}', 'StaffController@edit');
Route::post('/home/staffs/edit/{id}', 'StaffController@update')->name('update.staff');
Route::delete('/home/staffs/destroy/{id}', 'StaffController@destroy')->name('delete.staff');

Route::get('/home/experience', 'ExperienceController@index');
// Route::get('/home/experience/create', 'ExperienceController@create');
// Route::post('/home/experience/create', 'ExperienceController@store');
Route::get('/home/experience/edit/{id}', 'ExperienceController@edit')->name('experience.edit');
Route::post('/home/experience/edit/{id}', 'ExperienceController@update')->name('experience.update');
Route::delete('/home/experience/destroy/{id}', 'ExperienceController@destroy')->name('experience.delete');